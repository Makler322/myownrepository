import matplotlib.pyplot as plt
from matplotlib import rc
font = {'family': 'Verdana'}
rc('font', **font)

Vstat=[] #Создаём вспомогательный массив, в котором будем вести учёт о заболвевших
X=[] #Массив в котором считаю дни
Y=[] #Массив в котором отображаю кол-во нетронутых эпидемией людей
C=[] #Массив в котором отображаю кол-во больных на данный момент
L=1000 #Общая численность жителей
K=0.5 #Коффициент роста
N=1 #Число заболевших изначально
W=0 #Кол-во переболевших людей, изначально 0


def euler(i,W,N): #Функция Эйлера
    if i>6: #Люди которые выздоровели, если речь идёт о первой недели, то люди там не вызоравливали
        V=Vstat[i-7]
    else:
        V=0
    W+=V #К переболевшим добавляются выздоровшие
    Z=(K*N*((L-N-W)/L))#Кол-во заболевших в этот день
    Vstat.append(Z) #Добаляем в "отчет" кол-во заболевших в данный день
    N=N+Z-V #Число больных при эпидемии
    return W,N

i=-1 #Ввожу счётчик дней
while 1!=0:
    i+=1
    W,N=euler(i,W,N) #Кол-во переболевших людей и больных в данный день
    Y.append(L-W) #Редактируем наши массивы
    X.append(i)
    C.append(N)
    if i>7 and round(Y[i])==round(Y[i-7]): #Часть кодаотечающая за конец эпидемии. Если за 7 дней никто не заразился, то эпидемия окончена
        print(i-6) #Так как в моей программе есть нулевой день, то i значение я вывожу не -7, а -6
        break

print(round(L-Y[i-6]),round(Y[i-6])) #Вывожу кол-во переболевших и кол-во тех, кого эпидемия вообще не тронула
print(round(max(C))) #Максимальное кол-во больных


graph = plt.figure() #Графический вывод
ax = graph.add_subplot(111)

plt.grid(True)  #Сеточка
plt.title('Графики изменения численности бурундуков и белок')  #Заголовок
plt.xlabel('t, лет')
plt.ylabel('n, особей')
line_1, = ax.plot(X, Y,'r')
line_2, = ax.plot(X, C,'yellow')
plt.legend([line_1, line_2], ['Белки', 'Бурундуки']) #Квадратик
plt.show()