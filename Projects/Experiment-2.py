import matplotlib.pyplot as plt

X=[0] #Массив времени (координта х)
Y=[82, 80.5, 79.5, 78.0, 77.0, 76.5, 75.5, 75.0, 74.0, 73.0, 72.5, 72.5, 72, 71.5, 71.0, 70.0, 70.0, 69.0, 69.0, 68.0, 68.0, 67.0, 66.5, 66.0, 66.0, 65.5, 65.0, 64.5, 64.0, 63.0] #Ручками вводим эксперементальные данные
Y1=[Y[0]] #Массив аналитических данных
Tstart=Y[0]  #Начальная температура кофе
Tk=26 #Комнатная температура
r=0.0297 #Коэффициент r, посчитанный нами ранее
dT=0.5 #Шаг времени

def euler(Tnew): #Алгоритм Эйлера
    Tnew=Tnew-r*(Tnew-Tk)*dT
    return Tnew

for i in range(1,len(Y)): #Основная программа, подсчитывающая температуру в каждый момент времени
    X.append(i)
    Y1.append(euler(Tstart))
    Tstart=euler(Tstart)

graph = plt.figure() # Графический вывод
ax = graph.add_subplot(111)
plt.xlabel('Minutes')
plt.ylabel('Blue - Theoretical data, Red - Experimental data')
ax.plot(X,Y,'r')
ax.plot(X,Y1,'b')
plt.show()