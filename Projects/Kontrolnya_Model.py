import numpy as np
from matplotlib import pyplot as plt
from matplotlib import animation
from random import random

K=100 #Кол-во точек
t=1/200
X=[]
Y=[]
VX=[]
VY=[]
# First set up the figure, the axis, and the plot element we want to animate
fig = plt.figure()
ax = plt.axes(xlim=(0, 2), ylim=(0, 2))
line, = ax.plot([], [], 'o',ms=3)

def tochka():
    x=random()*2-1
    y=random()*2-1
    Vx=random()*2-1
    Vy=random()*2-1
    return x,y,Vx,Vy

def delta_tochka(x,y,Vx,Vy,t):
    x=x+Vx*t
    y=y+Vy*t
    if x>=2:
        x=x-2
    elif x<=0:
        x=x+2
    if y>=2:
        y=y-2
    elif y<=0:
        y=y+2
    return x,y

def Get(L):
    XI,YI = [],[]
    for i in range(K):
        xi, yi = delta_tochka(X[i], Y[i], VX[i], VY[i], t)
        X[i] = xi
        Y[i] = yi
        XI.append(xi)
        YI.append(yi)
    return XI,YI


# initialization function: plot the background of each frame
def init():
    line.set_data([], [])
    return line,

# animation function.  This is called sequentially
def animate(i):
    x,y = Get(i)
    line.set_data(x, y)
    return line,

# call the animator.  blit=True means only re-draw the parts that have changed.
anim = animation.FuncAnimation(fig, animate, init_func=init,
                               frames=200, interval=20, blit=True)


#anim.save('basic_animation.mp4', fps=30, extra_args=['-vcodec', 'libx264'])
for j in range(K):
    x, y, Vx, Vy = tochka()
    X.append(x)
    Y.append(y)
    VX.append(Vx)
    VY.append(Vy)
XI=[]
YI=[]
Get_x=[]
Get_y=[]



plt.show()