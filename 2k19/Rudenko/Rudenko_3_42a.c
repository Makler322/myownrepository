#include <stdio.h>
#include <math.h>
int main(){
    double S=0, b, x=1.0;
    int n, i, j;
    scanf("%lf", &x);
    scanf("%d", &n);
    for (i=1; i<=n; i++){
        b=1.0;
        for (j=1; j<=n; j++){
            b *= sin(x);
        }
        S += b;
    }
    printf("%lf \n", S);
    return 0;
}