#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/fcntl.h>
#include <sys/wait.h>
#include <sys/types.h>

int main(int argc, char **argv){
    if(fork())
    {
        int * status;
        wait(status);
            int fd[2];
            pipe(fd);
            if (fork() == 0)//prog3
            {
                close(fd[0]);
                dup2(fd[1], 1);
                close(fd[1]);
                execlp(argv[3], argv[3], (char*)0);
            }
            close(fd[1]);
            if (fork() == 0)
            {
                dup2(fd[0], 0);
                close(fd[0]);
                execlp(argv[4], argv[4], (char*)0);
            }
            close(fd[0]);

            for (int i = 0; i < 2; i++)
                wait(0);
    }
    else
    {
        int fd1 = open(argv[2], O_WRONLY | O_CREAT, 0666);
        dup2(fd1, 1);
        close(fd1);
        execlp(argv[1], argv[1], (char*)0); //xподмена тела
    }
    
    return;
}