#include <stdio.h>
int a[ 3 ][ 3 ] = { { 228, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
int *pa[ 3 ] = { a[ 0 ], a[ 1 ], a[ 2 ] };
int *p = a[ 0 ];
main(){ 
    int i;
    for ( i = 0; i < 3; i ++ )
        printf(" a[ i ][ 2 – i ]=%d *a[ i ]=%d *(*(a+i)+i)=%d\n", a[i][2-i], *a[ i ], *(*(a+i)+i));
    for ( i = 0; i < 3; i ++ )
        printf("*pa[ i ]=%d p[ i ]=%d \n", *pa[ i ], p[ i ] );
}