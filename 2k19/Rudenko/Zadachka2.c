#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/fcntl.h>
#include <sys/wait.h>
#include <sys/types.h>

int main(int argc, char **argv){
    int fd1 = open(argv[1], O_RDONLY);
    int fdch1[2];
    int fdch2[2];
    int flag = 2;
    pipe(fdch1);
    pipe(fdch2);
    if (fork())
    {
        close(fdch1[0]);// close reading from channel
        close(fdch2[1]);// 
        char buf[atoi(argv[4])];
        int fd2 = open(argv[2], O_WRONLY);
        int n_read = 1;
        while(n_read){
            int n_ch_r = read(fdch2[0], &flag, sizeof(int));
            n_read = read(fd1, buf, atoi(argv[4]));
            write(fd2, buf, atoi(argv[4]));
            write(fdch1[1], &flag, sizeof(int));
        }
        close(fd2);
        close(fdch1[1]);
        close(fdch2[0]);
        wait(0);
    }
    else
    {
        close(fdch1[1]);
        close(fdch2[0]);
        char buf[atoi(argv[4])];
        int fd3 = open(argv[3], O_WRONLY);
        int n_read = 1;
        write(fdch2[1], &flag, sizeof(int));
        while(n_read){
            int n_ch_r = read(fdch1[0], &flag, sizeof(int));
            n_read = read(fd1, buf, atoi(argv[4]));
            write(fd3, buf, atoi(argv[4]));
            write(fdch2[1], &flag, sizeof(int));
        }
        close(fd3);
        close(fdch1[0]);
        close(fdch2[1]);
    }
    
}