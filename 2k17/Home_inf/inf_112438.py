n = int(input())
S1, S2, S3, S4 = [], [], [], []
min1, min2, min3, min4, = 10**8 + 12, 10**8 + 13, 10**8 + 13, 10**8 + 13
X1, Y1, X2, Y2, X3, Y3, X4, Y4 = 0, 0, 0, 0, 0, 0, 0, 0
for i in range(n):
    x, y = map(int, input().split())
    if x > 0 and y > 0:
        S1.append([x, y])
        if abs(y) < min1 or abs(x) < min1:
            min1 = min(min1, abs(y), abs(x))
            X1 = x
            Y1 = y
    elif x < 0 and y > 0:
        S2.append([x, y])
        if abs(y) < min2 or abs(x) < min2:
            min2 = min(min2, abs(y), abs(x))
            X2 = x
            Y2 = y
    elif x < 0 and y < 0:
        S3.append([x, y])
        if abs(y) < min3 or abs(x) < min3:
            min3 = min(min3, abs(y), abs(x))
            X3 = x
            Y3 = y
    elif x > 0 and y < 0:
        S4.append([x, y])
        if abs(y) < min4 or abs(x) < min4:
            min4 = min(min4, abs(y), abs(x))
            X4 = x
            Y4 = y

d = [[len(S1), min1, 1, X1, Y1], [len(S2), min2, 2, X2, Y2], [len(S3), min3, 3, X3, Y3], [len(S4), min4, 4, X4, Y4]]
J = [len(S1), len(S2), len(S3), len(S4)]
j1 = max(J)
K = [[len(S1), min1, 1, X1, Y1]]
if J.count(j1) > 1:
    JJ = []
    for i in d:
        if i[0] == j1:
            JJ.append(i[1])
    j2 = min(JJ)
    if JJ.count(j2) > 1:
        for i in d:
            if i[0] == j1 and i[1] == j2:
                K = i
                break
    else:
        for i in d:
            if i[0] == j1 and i[1] == j2:
                K = i
else:
    for i in d:
        if i[0] == j1:
            K = i


if K[2] == 1:
    l = S1
elif K[2] == 2:
    l = S2
elif K[2] == 3:
    l = S3
elif K[2] == 4:
    l = S4
print('K =', K[2])
print('M =', len(l))
print('A = ', '(', K[3], ', ',K[4],')', sep='')
print('R =', K[1])
exit()