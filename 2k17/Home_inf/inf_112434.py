n = int(input())
d ={}
for i in range(n):
    a, b = map(int, input().split())
    if b % a != 0:
        if b % a not in d:
            d[b % a] = 1
        else:
            d[b % a] += 1
if len(d) == 0:
    print(0)
    exit()
dict = list(d.items())
S1 = []
S2 = []
for i in dict:
    S1.append(i[0])
    S2.append(i[1])
c = max(S2)
C = []
for i in range(len(S2)):
    if S2[i] == c:
        C.append(S1[i])
print(max(C))
