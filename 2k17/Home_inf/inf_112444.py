n = int(input())
S = []
G = []
for i in range(n):
    A = input().split()
    summ = int(A[-1]) + int(A[-2]) + int(A[-3])
    S.append([A[0], A[1]])
    G.append(summ)
c = max(G)
for i in range(len(G)):
    if G[i] == c:
        print(' '.join(map(str,S[i])))
