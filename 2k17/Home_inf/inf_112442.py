A = input()
n = int(input())
d = {}
for i in range(n):
    B = input()
    C = B.split()
    D = C[0][0] + C[1][0] + C[2][0]
    if D == A:
        if B not in d:
            d[B] = 1
        else:
            d[B] += 1

for i in sorted(d.items(), key=lambda item: (-item[1], item[0])):
    print(' '.join(map(str, i)))