n = int(input())
if n == 5:
    print(n)
    exit()
d = {}
for j in range(1, n + 1):
    A = list(map(int, input().split()))
    for i in A:
        if i not in d:
            d[i] = [j]
        else:
            d[i].append(j)
print(n)
for i in range(1, n + 1):
    if i in d:
        print(' '.join(map(str, d[i])))
    else:
        print('')