from collections import deque as de
A = input().split('.')
d = {}
for i in A[0]:
    if i not in d:
        d[i] = 1
    else:
        d[i] += 1
dict = list(d.items())
count = 0
S = de()
for i in dict:
    if int(i[1]) % 2 != 0:
        count += 1
        S.append(i[0])
if count > 1:
    print('NO')
    exit()

dic = list(sorted(d.items(), key=lambda item: item[0]))
dic.reverse()
for i in dic:
    for j in range(int(i[1])//2):
        S.append(i[0])
        S.appendleft(i[0])
print('YES')
print(''.join(map(str, S)))