import matplotlib.pyplot as plt
k=float(input())
step=1
X=[]
Y=[]
def pr(x):
    return 2*x
for i in range(7):
    x=1
    y=1
    step*=0.1
    step=round(step,8)
    while x<k:
        y+=pr(x)*step
        x+=step
        x=round(x,8)
        y=round(y,8)
        if step==0.2:
            print(x,y)
    X.append(step)
    Y.append(y)
graph = plt.figure()
ax = graph.add_subplot(111)
ax.plot(X,Y)
print(X)
print(Y)
plt.show()