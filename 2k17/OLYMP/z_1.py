A = input()
B = [0] * 26
#print(B)
for i in A:
    #print(ord(i))
    if ord(i) < 96:

        B[ord(i) + 32 - 97] += 1
    else:
        B[ord(i) - 97] += 1
K = max(B)
for i in range(len(B)):
    if B[i] == K:
        print(chr(i + 97), K)
        break