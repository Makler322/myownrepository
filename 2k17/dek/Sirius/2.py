n, k, m = map(int, input().split())
N = list(map(int,input().split()))
A = N[1:m]
B = N[m-1:]
B.reverse()
POPA=N[m-1]
POPB=N[m-1]
while len(A)>k and len(B)>k:
    A1=A[:k]
    A2=A[k:]
    p=min(A1)
    for i in range(len(A1)):
        if A1[i]==p:
            A1.pop(i)
            POPA+=p
            break
    A=A1+A2
    B1=B[:k]
    B2=B[k:]
    p1=min(B1)
    for j in range(len(B1)):
        if B1[j] == p1:
            B1.pop(j)
            POPB += p1
            break
    B=B1+B2
print(min(POPA,POPB))