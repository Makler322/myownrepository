m, n = map(int,input().split())

#Функция для разложения числа
def check(k):
    for i in range(2,k):
        if k % i == 0:
            break
    return i, k//i

#Проверка на "отрицательность"
if m < 0:
    M = [-1]
    m = -m
else:
    M = []

if n < 0:
    N = [-1]
    n = -n
else:
    N = []

#Работа с первым числом
if m < 3:
    M.append(m)
else:
    while check(m)[0] < m - 1:
        km, m = check(m)
        M.append(km)
    M.append(m)

#Работа со вторым числом
if n < 3:
    N.append(n)
else:
    while check(n)[0] < n - 1:
        kn, n = check(n)
        N.append(kn)
    N.append(n)
flag = 1
while flag == 1:
    flag = 0
    for i in range(len(N)):
        for j in range(len(M)):
            if N[i] == M[j]:
                N.pop(i)
                M.pop(j)
                flag = 1
                break
        if flag == 1:
            break
m, n = 1, 1
for i in range(len(N)):
    n *= N[i]
for j in range(len(M)):
    m *= M[j]
print(m, n)