a=input().split()
b=input().split()
minim=30000000085
maxim=-30000000085
for i in range(len(a)):
    a[i]=int(a[i])
    if a[i]>maxim:
        maxim=a[i]
    if a[i]<minim:
        minim=a[i]
for i in range(len(b)):
    b[i]=int(b[i])
    if b[i]>maxim:
        maxim=b[i]
    if b[i]<minim:
        minim=b[i]
k=0
if minim==maxim:
    k=10**6
while len(a)!=0 and len(b)!=0 and k!=10**6:
    if a[0]==minim and b[0]==maxim:
        a.append(a[0])
        a.append(b[0])
        a = a[1:]
        b = b[1:]
    elif b[0]==minim and a[0]==maxim:
        b.append(a[0])
        b.append(b[0])
        a = a[1:]
        b = b[1:]
    elif a[0]>b[0]:
        a.append(a[0])
        a.append(b[0])
        a=a[1:]
        b=b[1:]
    elif b[0]>a[0]:
        b.append(a[0])
        b.append(b[0])
        a=a[1:]
        b=b[1:]
    k+=1
if k!=10**6:
    if len(a)==0:
        print('second',k)
    else:
        print('first',k)
else:
    print('botva')