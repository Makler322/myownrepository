from bisect import bisect_left

def binary_search(a, x, lo=0, hi=None):  # can't use a to specify default for hi
    hi = hi if hi is not None else len(a)  # hi defaults to len(a)
    pos = bisect_left(a, x, lo, hi)  # find insertion position
    return (pos if pos != hi and a[pos] == x else -1)  # don't walk off the end
N=int(input())
A=[-9]
for i in range(N):
    k=int(input())
    if k!=A[len(A)-1]:
        A.append(k)
M=int(input())
B=[]
k=0
for i in range(M):
    l=int(input())
    for o in range(len(A)):
        if binary_search(A,l,0)!=-1:
            k+=1
            break
print(k)