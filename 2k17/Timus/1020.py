import math as m
N, R = map(float,input().split())
N = int(N)
L=0
A, B, Angle = [], [], []
if N==1:
    a, b = map(float,input().split())
    print(round(2*3.14159*R,2))
else:
    for i in range(N):
        a, b = map(float,input().split())
        if len(A)!=0:
            if a != A[-1] or b != B[-1]:
                A.append(a)
                B.append(b)
        else:
            A.append(a)
            B.append(b)
    if A[0]==A[-1] and B[0]==B[-1]:
        A.pop()
        B.pop()
    for i in range(len(A)):
        if i == len(A)-1:
            Side1 = ((A[i]-A[i-1])**2+(B[i]-B[i-1])**2)**0.5
            Side2 = ((A[i]-A[0])**2+(B[i]-B[0])**2)**0.5
            Side3 = ((A[0]-A[i-1])**2+(B[0]-B[i-1])**2)**0.5
        else:
            Side1 = ((A[i]-A[i-1])**2+(B[i]-B[i-1])**2)**0.5
            Side2 = ((A[i]-A[i+1])**2+(B[i]-B[i+1])**2)**0.5
            Side3 = ((A[i-1]-A[i+1])**2+(B[i-1]-B[i+1])**2)** 0.5
        L+=3.14159*R*(1 - ((m.acos((Side1**2+Side2**2-Side3**2)/(2*Side1*Side2))*57.3)/180))
        L+=Side2
    print(round(L,2))
