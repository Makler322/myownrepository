import sys, math
_in = sys.stdin.read()

for nr in _in.split()[::-1]:
    print ("%.4f" % math.sqrt(float(nr)))
