a=int(input())
if a>0 and a<=4:
    print('few')
elif a<=9:
    print('several')
elif a<=19:
    print('pack')
elif a<=49:
    print('lots')
elif a<=99:
    print('horde')
elif a<=249:
    print('throng')
elif a<=499:
    print('swarm')
elif a<=999:
    print('zounds')
else:
    print('legion')