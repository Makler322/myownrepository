import collections as coll


N, M = map(int, input().split())
K = int(input())

def check(a,b):
    global N, M
    if 0 <= a <= N and 0 <= b <= M:
        return True
    else:
        return False

if K != 0:
    a, b = map(int, input().split())
    setik = {[a,b]}
    for i in range(K - 1):
        a, b = map(int, input().split())
        setik.add([a,b])



deck = coll.deque()
deck.append([1, 1])

while len(deck) != 0:
    if check(deck[0][0] + 1, deck[0][1]):
        deck.append([deck[0][0] + 1, deck[0][1]])

    if check(deck[0][0], deck[0][1] + 1):
        deck.append([deck[0][0], deck[0][1] + 1])

    if deck[0] in setik:
        if check(deck[0][0] + 1, deck[0][1] + 1):
            deck.append([deck[0][0] + 1, deck[0][1] + 1])

    deck.popleft()