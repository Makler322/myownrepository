n, m =map(int,input().split())
p=n/m
c=abs(p-int(p))
if m>=n:
    print(2)
elif c>0 and c<=0.5:
    print(int(p)*2+1)
elif c>0.5:
    print(int(p)*2+2)
elif c==0:
    print(n*2//m)