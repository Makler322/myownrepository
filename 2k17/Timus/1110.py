N,M,Y=map(int,input().split())
S=[]
for i in range(M):
    if (i**N)%M==Y:
        S.append(i)
if len(S)==0:
    print(-1)
else:
    print(' '.join(map(str,S)))