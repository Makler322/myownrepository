A=input().split()
while 0!=1:
    B=[]
    k=0
    for i in range(len(A)):
        if A[i]=='+' or A[i]=='-' or A[i]=='*':
            if A[i-1]!='+' and A[i-1]!='-' and A[i-1]!='*' and A[i-2]!='+' and A[i-2]!='-' and A[i-2]!='*' and A[i-1]!=-228 and A[i-2]!=-228:
                if A[i]=='+':
                    A[i]=int(A[i-1])+int(A[i-2])
                    A[i-1]=-228
                    A[i-2]=-228
                    B.pop()
                    B.pop()
                    B.append(A[i])
                elif A[i]=='-':
                    A[i]=int(A[i-2])-int(A[i-1])
                    A[i-1]=-228
                    A[i-2]=-228
                    B.pop()
                    B.pop()
                    B.append(A[i])
                elif A[i]=='*':
                    A[i]=int(A[i-1])*int(A[i-2])
                    A[i-1]=-228
                    A[i-2]=-228
                    B.pop()
                    B.pop()
                    B.append(A[i])
            else:
                B.append(A[i])
        else:
            B.append(A[i])
    if len(B)!=1:
        A=B.copy()
    else:
        break
print(int(B[0]))