with open("input.txt",encoding='KOI8-R') as f:
   text=f.read()

A=text.lstrip().split('\n')
Numbers=[]
Names=[]

for i in range(len(A)):
    a=A[i].split()
    if len(a) < 2:
        break
    Numbers.append(int(a[0]))
    Names.append(a[1])
Copy=Numbers.copy()
Copy.sort()


output = open('output.txt',mode = 'w',encoding='KOI8-R')

for i in range(len(Copy)):
    for j in range(len(Copy)):
        if Copy[i]==Numbers[j]:
            output.write(str(Numbers[j]))
            output.write(str(' '))
            output.write(Names[j])
            output.write('\n')
            Numbers[j] = '-1'