import collections as coll
a, b = map(int,input().split())
c, d = map(int,input().split())
count = 8

def check(a,b):
    global Chess, count
    return not (a < 0 or b < 0 or a > count - 1  or b > count - 1) and Chess[a][b]

deck = coll.deque()
deck.append([a,b])

Chess = [[True for i in range(count)] for i in range(count)]
Chess[a][b] = False

Pre = [[[-1, -1] for i in range(count)] for i in range(count)]

dx = [1, 1, 2, 2, -1, -1, -2, -2]
dy = [2, -2, 1, -1, 2, -2, 1, -1]


while Chess[c][d]:
    t1, t2 = deck[0]
    for i, j in zip(dx, dy):
        if check(t1 + i, t2 + j):
            deck.append([t1 + i, t2 + j])
            Chess[t1 + i][t2 + j] = False
            Pre[t1 + i][t2 + j] = [t1, t2]
    deck.popleft()

answer = []
Itog = [c,d]
while  Itog != [a,b]:
    answer.append(Itog)
    i1, i2 = Itog
    Itog = Pre[i1][i2]

answer.append([a,b])
answer.reverse()

for i in range(len(answer)):
    print(' '.join(map(str, answer[i])))
print(len(answer)-1)
