a, b = map(int,input().split())
A, B = [], []
def XOR(a,b):
    x0 = max(a,b)
    y0 = min(a,b)
    x = bin(x0)[2:]
    y = bin(y0)[2:]
    if len(y) < len(x):
        y=y.zfill(len(x))
    k = 0
    count = []
    ut = '0b'
    #count = count.zfill(len(x))
    #print(count,type(count))
    for i in range(len(x)):
        if x[i] != y[i] :
            ut += '1'
        else:
            ut += '0'
        count_int = int(ut,2)
        count = bin(count_int)[2:]

    for i in range(1,len(count)):
        if count[i] == '0':
            k += 1
    if count[0] == 1 and k == len(count)-1:
        return -1
    else:
        return 1
A.append(a)
for i in range(a+1,b+1):
    flag = 0
    for j in A:
        if XOR(i, j) == -1:
            B.append(i)
            flag = 1
            break
    if flag == 0:
        A.append(i)
print(A,B)