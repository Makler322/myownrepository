'''
Формат ввода, на примере:
6
Ivanov 15
Grigori 11
Hey 10
Hello 14
Grig 9
Guli 13
'''
N = int(input()) #Кол-во людей
Name, Ball = [], [] #Будем работать с массивом имён и чисел
max_1 = False
max_2 = False
max_3 = False
for i in range(N): #Вводим данные
    Data = list(map(str,input().split()))
    Name.append(Data[0]), Ball.append(int(Data[1]))
    if int(Data[1]) >= max_1: #Сразу находим максимум
        max_1 = int(Data[1])


for i in range(N): #Пробегаемся по массиву и выводим первое место
    if Ball[i] == max_1 or Ball[i] == max_1 - 1:
        print(Name[i], '1 место')
    elif Ball[i] >= max_2: #Среди оставшихся находим второй максимум
        max_2 = Ball[i]


for i in range(N): #Аналогично выводим второе место
    if Ball[i] == max_2 or Ball[i] == max_2 - 1:
        print(Name[i], '2 место')
    elif Ball[i] >= max_3 and Ball[i] < max_2 - 1: #Находим третий максимум  при условии, что он меньше второго
        max_3 = Ball[i]


for i in range(N): #Пробегаемся и выводим третие место
    if Ball[i] == max_3 or Ball[i] == max_3 - 1:
        print(Name[i], '3 место')
