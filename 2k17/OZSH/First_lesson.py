from PIL import Image #Импортируем библиотеку PIL
img = Image.open ('White.jpg').convert('RGB') #Берём и открываем фото
H,L = img.size #Высота и длина Отца
img2 = Image.open("Dark.jpg")
h2,l2 = img2.size #Высота и длина Сына

for i in range(0, (H-1)//2):  # Пробегаемся по высоте и длине Сына ровно до половины в Отце
    for j in range(0, (L-1)//2):

        R, G, B = img2.getpixel((i,j)) #Son's data
        R_bin = bin(R)[:2]+bin(R)[2:].zfill(8)
        G_bin = bin(G)[:2]+bin(G)[2:].zfill(8)
        B_bin = bin(B)[:2]+bin(B)[2:].zfill(8)
        #print(B_bin)

        R1, G1, B1 = img.getpixel((2*i, 2*j)) #Dady's data
        R1_bin = bin(R1)[:2] + bin(R1)[2:].zfill(8)
        G1_bin = bin(G1)[:2] + bin(G1)[2:].zfill(8)
        B1_bin = bin(B1)[:2] + bin(B1)[2:].zfill(8)
        print(B1_bin)

        R1_bin = R1_bin[:8]+R_bin[2:4]
        G1_bin = G1_bin[:8]+G_bin[2:4]
        B1_bin = B1_bin[:8]+B_bin[2:4]
        img.putpixel((2*i, 2*j),(int(R1_bin, 2), int(G1_bin, 2), int(B1_bin,2)))

        R1, G1, B1 = img.getpixel((2*i, 2*j+1))
        R1_bin = bin(R1)[:2] + bin(R1)[2:].zfill(8)
        G1_bin = bin(G1)[:2] + bin(G1)[2:].zfill(8)
        B1_bin = bin(B1)[:2] + bin(B1)[2:].zfill(8)

        R1_bin = R1_bin[:8] + R_bin[4:6]
        G1_bin = G1_bin[:8] + G_bin[4:6]
        B1_bin = B1_bin[:8] + B_bin[4:6]
        img.putpixel((2*i, 2*j+1), (int(R1_bin, 2), int(G1_bin, 2), int(B1_bin,2)))

        R1, G1, B1 = img.getpixel((2*i+1, 2*j))
        R1_bin = bin(R1)[:2] + bin(R1)[2:].zfill(8)
        G1_bin = bin(G1)[:2] + bin(G1)[2:].zfill(8)
        B1_bin = bin(B1)[:2] + bin(B1)[2:].zfill(8)

        R1_bin = R1_bin[:8] + R_bin[6:8]
        G1_bin = G1_bin[:8] + G_bin[6:8]
        B1_bin = B1_bin[:8] + B_bin[6:8]
        img.putpixel((2*i+1, 2*j), (int(R1_bin, 2), int(G1_bin, 2), int(B1_bin,2)))

        R1, G1, B1 = img.getpixel((2*i+1, 2*j+1))
        R1_bin = bin(R1)[:2] + bin(R1)[2:].zfill(8)
        G1_bin = bin(G1)[:2] + bin(G1)[2:].zfill(8)
        B1_bin = bin(B1)[:2] + bin(B1)[2:].zfill(8)

        R1_bin = R1_bin[:8] + R_bin[8:10]
        G1_bin = G1_bin[:8] + G_bin[8:10]
        B1_bin = B1_bin[:8] + B_bin[8:10]
        img.putpixel((2*i+1, 2*j+1), (int(R1_bin, 2), int(G1_bin, 2), int(B1_bin,2)))

img.save('result.jpg')
img.show() #Показываем