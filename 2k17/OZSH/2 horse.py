import collections as coll

x1_start, y1_start = map(int,input().split()) #input data
x2_start, y2_start = map(int,input().split())
x1_end, y1_end = map(int,input().split())
x2_end, y2_end = map(int,input().split())

deck = coll.deque()
deck.append([x1_start, y1_start, x2_start, y2_start])

Size = 8 #Chess size
Used = [[[[True for i in range(Size)] for i in range(Size)] for i in range(Size)] for i in range(Size)]
Pre = [[[[[False, False, False, False] for i in range(Size)] for i in range(Size)] for i in range(Size)] for i in range(Size)]

def check(S):
    global Used, Size
    for i in S:
        if i < 0 or i >= Size:
            return False
    return Used[S[0]][S[1]][S[2]][S[3]] and ([S[0],S[1]] != [S[2],S[3]])

dx1 = [1, 1, 2, 2, -1, -1, -2, -2]
dy1 = [2, -2, 1, -1, 2, -2, 1, -1]
dx2 = [1, 1, 2, 2, -1, -1, -2, -2]
dy2 = [2, -2, 1, -1, 2, -2, 1, -1]

x0_1, y0_1, x0_2, y0_2 = deck[0]

while len(deck) != 0 and [x0_1, y0_1, x0_2, y0_2] != [x1_end, y1_end, x2_end, y2_end]:
    #print(0)
    x0_1, y0_1, x0_2, y0_2 = deck[0]
    for i1, j1 in zip(dx1,dy1):
        for i2, j2 in zip(dx2, dy2):
            if check([x0_1 + i1, y0_1 + j1, x0_2 + i2, y0_2 + j2]):
                deck.append([x0_1 + i1, y0_1 + j1, x0_2 + i2, y0_2 + j2])
                Used[x0_1 + i1][y0_1 + j1][x0_2 + i2][y0_2 + j2] = False
                Pre[x0_1 + i1][y0_1 + j1][x0_2 + i2][y0_2 + j2] = [x0_1, y0_1, x0_2, y0_2]
    deck.popleft()
if [x0_1, y0_1, x0_2, y0_2] != [x1_end, y1_end, x2_end, y2_end]:
    print('Impossible')
    exit(0)
answer = []
Itog = [x1_end, y1_end, x2_end, y2_end]
while  Itog != [x1_start, y1_start, x2_start, y2_start]:
    #print(Itog, [x1_start, y1_start, x2_start, y2_start], answer)
    answer.append(Itog)
    x1, y1, x2, y2 = Itog
    Itog = Pre[x1][y1][x2][y2]

answer.append([x1_start, y1_start, x2_start, y2_start])
answer.reverse()

for i in range(len(answer)):
    print(' '.join(map(str, answer[i])))
print(len(answer)-1)
'''
1 4
5 7
6 3
0 2
'''