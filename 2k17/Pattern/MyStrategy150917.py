
class Hero():
    def __init__(self):
        self.behavior_strategy = None

    def set_behavior_strategy(self, strategy):
        self.behavior_strategy = strategy


    def perform_behavior(self):
        self.behavior_strategy.action()



class action_behavior(): #Интерфейс
    def action(self):
        raise NotImplementedError()


class support(action_behavior):
    def action(self):
        print("I wll help you even if I die")

class provocation(action_behavior):
    def action(self):
        print("I wll take the damage for themselves")

class invisibility(action_behavior):
    def action(self):
        print("I wll hide and will appear in the most important moment")


class Intelligence(Hero):
    def __init__(self):
        self.behavior_strategy = support()
    def display(self):
        pass


class Strength(Hero):
    def __init__(self):
        self.behavior_strategy = provocation()
    def display(self):
        pass



class Dexterity(Hero):
    def __init__(self):
        self.behavior_strategy = invisibility()
    def display(self):
        pass

if __name__ == '__main__':
    Dexterity = Dexterity()
    Strength = Strength()
    Intelligence = Intelligence()

    Dexterity.perform_behavior()
    Strength.perform_behavior()
    Intelligence.perform_behavior()
