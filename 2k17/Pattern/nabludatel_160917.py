from random import random


class Subject:
    def register_observer(self):  # Зарегистрировать объект
        raise NotImplementedError()

    def remove_observer(self):  # Удалить объект
        raise NotImplementedError()

    def notify_observer(self):  # Оповестить об показании температуры
        raise NotImplementedError()


class Observer:  # Интерфейс
    def update(self, s):
        raise NotImplementedError()


class DisplayElement:  # Интерфейс
    def display(self):
        raise NotImplementedError()

#class Weather_station():
    
class CurrentCondition(Observer, DisplayElement):  #
    def __init__(self):  # Инициализирует переменную
        self.s = None

    def update(self, s):  # Обнвляет переменные
        k = random()
        if k < 0.5:
            self.s = - s * 20
        else:
            self.s = s * 20

        self.Vlaznost = self.s * random()
        if self.Vlaznost < 70:
            self.Vlaznost += 70
    #def Prognoz(self):
        self.minn = 1 - random()*random()
        self.maxx = 1 + random()*random()
        self.sred = (self.minn + self.maxx)/2

    def display(self):  # Выводит на экран переменные
        print('Температура: ', self.s)
        print('Влажность: ', self.Vlaznost, '%')  # Перевожу в проценты
        print('Атмосферное давление: ', 760 + self.s * random() * 2, 'мм. рт. ст.')
        print('Средняя/Минимальная/Максимальная Температура: ', self.s * self.sred,'/ ', self.s * self.minn, '/ ', self.s * self.maxx)
        print('Средняя/Минимальная/Максимальная Влажность: ', self.Vlaznost * self.sred, '%/ ', self.Vlaznost * self.minn, '%/ ',
              100 - random()*5, '%')
        print('Средняя/Минимальная/Максимальная Атмосферное давление: ', (760 + self.s * random() * 2) * self.sred, (760 + self.s * random() * 2) * self.minn,
              (760 + self.s * random() * 2) * self.maxx)
        self.Vlaznost = self.s * random()
        if self.Vlaznost < 70:
            self.Vlaznost += 70
        self.minn = 1 - random() * random()
        self.maxx = 1 + random() * random()
        self.sred = (self.minn + self.maxx) / 2



    def display(self):  # Выводит на экран переменные
        print('Температура: ', self.s)
        print('Влажность: ', self.Vlaznost, '%')  # Перевожу в проценты
        print('Атмосферное давление: ', 760 + self.s * random() * 2, 'мм. рт. ст.')
        print('Средняя/Минимальная/Максимальная Температура: ', self.s * self.sred, '/ ', self.s * self.minn, '/ ',
              self.s * self.maxx)
        print('Средняя/Минимальная/Максимальная Влажность: ', self.Vlaznost * self.sred, '%/ ',
              self.Vlaznost * self.minn, '%/ ',
              100 - random() * 5, '%')
        print('Средняя/Минимальная/Максимальная Атмосферное давление: ', (760 + self.s * random() * 2) * self.sred,
              (760 + self.s * random() * 2) * self.minn,
              (760 + self.s * random() * 2) * self.maxx)
    def Prognoz(self):
        print('Прогноз:')
        if self.s > 15:
            print('На улице отличная погода!')
        elif self.s > 8:
            print('За окном хорошая погода, стоит проветриться.')
        elif self.s > 0:
            print('На улице чуть больше 0, однако это не повод сидеть дома)')
        elif self.s > -8:
            print('Погода такое себе, гуляй, но не долго!')
        else:
            print('Чёт слишком холодно, лучше посиди дома и порешай информатику.')


class WeatherData(Subject):
    def __init__(self):
        self.observers = []  # Создаем массив элементов
        self.s = None

    def register_observer(self, ob):  # Сверяем элемент по типу, если сходится, то добавляем
        if isinstance(ob, Observer):
            self.observers.append(ob)
        else:
            raise TypeError("{0} not is instance Observer".format(type(ob)))

    def remove_observer(self, ob):  # Удаляет объект
        self.observers.remove(ob)

    def notify_observer(self):  #
        for el in self.observers:
            el.update(self.s)

    def getTemperature(self):
        return random()

    def measurementsChanged(self):
        self.s = self.getTemperature()
        self.notify_observer()


if __name__ == "__main__":
    wd = WeatherData()
    cod = CurrentCondition()
    wd.register_observer(cod)
    wd.measurementsChanged()
    cod.display()
    cod.Prognoz()

    
