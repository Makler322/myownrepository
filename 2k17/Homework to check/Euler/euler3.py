import matplotlib.pyplot as plt

def function(x): # Функция y(x), по х возвращает у
    return x**2

def pr(x): # Функция возращающая производную от х**2
    return 2*x

def euler(x,y,step): #Алгоритм Эйлера
    x+=step
    y+=pr(x)*step
    return x,y


X,Y=[],[]
k=5
step=0
step0=0.1
okr=8

for i in range(7): # Осноная программа, меняющая шаг до 0,7
    x=1
    y=1
    step+=step0
    step=round(step,okr)
    while x<k:
        x,y=euler(x, y, step)[0], euler(x, y, step)[1]
    okr=8  # Округление до okr знака
    X.append(step)  # Добавляю в конечный массив только конечную разность, а не текущую
    Y.append(round(abs(y - function(x)), okr))
'''
for i in range(5): # Основная программа, меняющая шаг до 0,00001
    x=1
    y=1
    step0*=0.1
    step0=round(step0,okr)
    while x<k:
        x,y=euler(x, y, step)[0], euler(x, y, step)[1]
        okr=8  # Округление до okr знака
    X.append(step)  # Добавляю в конечный массив только конечную разность, а не текущую
    Y.append(round(abs(y - function(x)), okr))
'''
graph = plt.figure() # Графический вывод
ax = graph.add_subplot(111)
plt.xlabel('Step')
plt.ylabel('Value')
ax.plot(X,Y)
print(X)
print(Y)
plt.show()