import matplotlib.pyplot as plt

X=[0] #Массив времени (координта х)
Y=[83, 77.7, 75.1, 73, 71.1, 69.4, 67.8, 66.4, 64.7, 63.4, 62.1, 61, 59.9, 58.7, 57.8, 56.6] #Ручками вводим эксперементальные данные
Y1=[Y[0]] #Массив аналитических данных
Tstart=Y[0]  #Начальная температура кофе
Tk=22 #Комнатная температура
r=0.041 #Коэффициент r, посчитанный нами ранее
dT=1 #Шаг времени

def euler(Tnew): #Алгоритм Эйлера
    Tnew=Tnew-r*(Tnew-Tk)*dT
    return Tnew

for i in range(1,len(Y)): #Основная программа, подсчитывающая температуру в каждый момент времени
    X.append(i)
    Y1.append(euler(Tstart))
    Tstart=euler(Tstart)

graph = plt.figure() # Графический вывод
ax = graph.add_subplot(111)
plt.xlabel('Minutes')
plt.ylabel('Blue - Theoretical data, Red - Experimental data')
ax.plot(X,Y,'r')
ax.plot(X,Y1,'b')
plt.show()