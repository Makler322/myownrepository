def SelectionSort(A):
    for j in range(0, len(A) - 1):
        i_max = j
        for i in range(j, len(A)):
            if A[i] < A[i_max]:
                i_max = i
        A[j],A[i_max] = A[i_max], A[j]
    return A
a=list(map(int,input().split()))
b=list(map(int,input().split()))
a=SelectionSort(a)
b=SelectionSort(b)
s=0
for i in range(len(b)):
    for j in range(len(a)):
        if b[i]<=a[j]:
            s+=1
            a[j]=-1
            break
print(s)