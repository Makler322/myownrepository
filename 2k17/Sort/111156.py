def SelectionSort(A):
    for j in range(0, len(A) - 1):
        i_max = j
        for i in range(j, len(A)):
            if A[i] > A[i_max]:
                i_max = i
        A[j], A[i_max] = A[i_max], A[j]
    return A
a=list(map(int,input().split()))
a=SelectionSort(a)
print(' '.join(map(str,a)))