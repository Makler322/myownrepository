a=list(map(int,input().split()))
m1,m2,n1,n2=0,0,0,0
for i in range(len(a)):
    if a[i]>=m2 and a[i]<=m1:
        m2=a[i]
    elif a[i]>=m1:
        m2=m1
        m1=a[i]
    if a[i]<=n2 and a[i]>=n1:
        n2=a[i]
    elif a[i]<=n1:
        n2=n1
        n1=a[i]
if n2*n1>m2*m1:
    print(n1,n2)
else:
    print(m2,m1)