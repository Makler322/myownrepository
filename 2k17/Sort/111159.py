def SelectionSort(A):
    for j in range(0, len(A) - 1):
        i_max = j
        for i in range(j, len(A)):
            if A[i] < A[i_max]:
                i_max = i
        A[j],A[i_max] = A[i_max], A[j]
    return A
S,N=map(int,(input().split()))
H=[]
K=0
for i in range(N):
    a=int(input())
    K+=a
    H.append(a)
H=SelectionSort(H)
J=0
o=0
if K>S:
    while S>J:
        J+=H[o]
        o+=1
    if o!=0:
        print(o-1)
    else:
        print(0)
else:
    print(N)