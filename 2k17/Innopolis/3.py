import collections as coll
n, m = map(int, input().split())
S = []
for i in range(n):
    sr = list(map(int, input().split()))
    S.append(sr)

def check(S):
    global Used, Summ
    if S[0] < 0 or S[0] >= n or S[1] < 0 or S[1] >= m:
        return False
    if Summ < 0:
        return False
    return Used[S[0]][S[1]]

for i1 in range(n):
    for j1 in range(m):
        Flag = True
        if S[i1][j1] > 0:
            deck = coll.deque()
            deck.append((i1, j1))
            print(deck)
            [i, j] = deck[0]
            while len(deck) > 0 and Flag:
                Used = [[True for i in range(m)] for i in range(n)]
                Pre = [[[False, False] for i in range(m)] for i in range(n)]
                dx = [1, 0, 0, -1]
                dy = [0, 1, -1, 0]
                [i, j] = deck[0]
                for k1, k2 in zip(dx, dy):
                    if check([i + k1, j + k2]):
                        deck.append([[i + k1, j + k2]])
                        Used[i + k1][j + k2] = False
                        Pre[i + k1][j + k2] = [i, j]
                    else:
                        Flag = False
                Itog = deck[0]
                deck.popleft()
            if Flag:
                answer = []
                while Itog != [i1, j1]:
                    # print(Itog, [x1_start, y1_start, x2_start, y2_start], answer)
                    answer.append(Itog)
                    [i1, j1] = Itog
                    Itog = Pre[i1][j1]
                    answer.append(Itog)
                answer.reverse()
                print(answer)