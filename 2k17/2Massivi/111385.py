n,m,a=map(int,input().split())
S=[]
k=0
for i in range(n):
    S.append([0]*m)
for i in range(a):
    B=input().split()
    S[int(B[0])-1][int(B[1])-1]='*'
for i in range(n):
    for j in range(m):
        if S[i][j]!='*':
            if i-1>=0 and S[i-1][j]=='*':
                k+=1
            if i+1<n and S[i+1][j]=='*':
                k+=1
            if j-1>=0 and S[i][j-1]=='*':
                k+=1
            if j+1<m and S[i][j+1]=='*':
                k+=1
            if i-1>=0 and j-1>=0 and S[i-1][j-1]=='*':
                k+=1
            if i-1>=0 and j+1<m and S[i-1][j+1]=='*':
                k+=1
            if i+1<n and j-1>=0 and S[i+1][j-1]=='*':
                k+=1
            if i+1<n and j+1<m and S[i+1][j+1]=='*':
                k+=1
            S[i][j]=k
            k=0
for j in range(n):
    print(' '.join(map(str,S[j])))
