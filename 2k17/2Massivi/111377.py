n,m=map(int,input().split())
S=[]
for i in range(n):
    S.append([0]*m)
for i in range(n):
    if i%2==0:
        for j in range(m):
            S[i][j]=i*m+j+1
    else:
        for j in range(m):
            S[i][j]=m*(i+1)-j
for i in range(n):
    for j in range(m):
        if S[i][j]<10:
            print('  ',S[i][j],end='')
        elif S[i][j]<100:
            print(' ',S[i][j],end='')
        elif S[i][j]<1000:
            print('',S[i][j],end='')
        elif S[i][j]<10000:
            print(S[i][j], end='')
        else:
            print(S[i][j])
    print()
