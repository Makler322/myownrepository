n,m=map(int,input().split())
S=[]
for i in range(n):
    S.append([0]*m)
for i in range(n):
    for j in range(m):
        if i==0 or j==0:
            S[0][j]=1
            S[i][0]=1
        else:
            S[i][j]=S[i-1][j]+S[i][j-1]
for i in range(n):
    for j in range(m):
        if S[i][j]<10:
            print('    ',S[i][j],end='')
        elif S[i][j]<100:
            print('   ', S[i][j], end='')
        elif S[i][j] < 1000:
            print('  ', S[i][j], end='')
        elif S[i][j] < 10000:
            print(' ', S[i][j], end='')
        elif S[i][j] < 100000:
            print('', S[i][j], end='')
        elif S[i][j] < 1000000:
            print(S[i][j], end='')
        elif S[i][j] < 10000000:
            print(S[i][j])
    print()