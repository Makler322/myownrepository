n,m=map(int,input().split())
S=[]
for i in range(n):
    S.append([0]*m)
k=1
for i in range(n):
    for j in range(m):
        if i%2==j%2:
            S[i][j]=k
            k+=1
for i in range(n):
    for j in range(m):
        if S[i][j]<10:
            print('  ',S[i][j],end='')
        elif S[i][j]<100:
            print(' ',S[i][j],end='')
        elif S[i][j]<1000:
            print('',S[i][j],end='')
        elif S[i][j]<10000:
            print(S[i][j], end='')
        else:
            print(S[i][j])
    print()