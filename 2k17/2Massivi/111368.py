n=int(input())
S=[]
p=0
for i in range(n):
    S.append(list(map(int,input().split())))
for i in range(n):
    for j in range(n):
        if S[i][j]==S[j][i]:
            p+=1
if p==n**2:
    print('YES')
else:
    print('NO')