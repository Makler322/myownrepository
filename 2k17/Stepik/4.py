K = int(input())
N = K//4
afa = set()
for i in range(N):
    A = input()[:-1]
    B = input()[:-1]
    C = input()[:-1]
    D = input()[:-1]
    if A[-2] == ' ' or B[-2] == ' ' or C[-2] == ' ' or D[-2] == ' ':
        afa.add(0)
        afa.add(5)
    elif A[-2:] == B[-2:] and C[-2:] == D[-2:]:
        afa.add(1)

    elif A[-2:] == C[-2:] and B[-2:] == D[-2:]:
        afa.add(2)

    elif A[-2:] == D[-2:] and B[-2:] == C[-2:]:
        afa.add(3)

    elif A[-2:] == B[-2:] and C[-2:] == D[-2:]:
        afa.add(4)

if len(afa) == 1:
    print('Yes')
else:
    print('No')