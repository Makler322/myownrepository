from time import time
t1 = time()
file = open('input.txt', 'r')
answer = open('output.txt', 'w')
A, N = map(int,(file.readline().split()))

a = A + 1
b = - N
c = - A
if abs(a) > 10**6 - 10**5 - 10**4:
    answer.write("-1")
else:
    for k in range(abs(a)):
        if (c - b * k) % a == 0:
            y = k
            x = (c - b * y) // a
            answer.write(str(x))
            break
    else:
        answer.write("-1")
    #t2 = time()
#print(t2 - t1)