import sqlite3
conn = sqlite3.connect('products.db')  # Создание базы данных
c = conn.cursor()

drop_table_product = \
"""
DROP TABLE IF EXISTS product;

"""

create_table_product = \
"""
CREATE TABLE product(
id INTEGER,
name TEXT,
price FLOAT
);
"""

create_table_purchases = \
"""
CREATE TABLE purchases(
id INTEGER,
time TEXT,
id_shopper INTEGER,
id_product INTEGER,
number INTEGER,
);
"""

create_table_shopper = \
"""
CREATE TABLE product(
id INTEGER,
name TEXT,
address TEXT,
);
"""

list_table_name = \
"""
SELECT name FROM sqlite_master WHERE type = 'table';

"""

# Подготовка, создание "очереди"
c.execute(drop_table_product)
c.execute(create_table_product)


# Save (commit) the changes
conn.commit()
for row in c.execute(list_table_name):
    print(row)

c.execute('INSERT INTO media VALUES (NULL, x)')

# We can also close the connection if we are done with it.
# Just be sure any changes have been committed or they will be lost.
conn.close()