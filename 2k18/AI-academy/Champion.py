from IPython.display import display
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
from sklearn.model_selection import GridSearchCV
import pandas as pd
import matplotlib.pyplot as plt
import time
import os
import sklearn
if __name__ == '__main__':
    lef = open('answer.txt', 'w')
    plt.style.use('ggplot')

    x = pd.read_csv('train.csv', index_col=0)
    y = pd.read_csv('target.csv', index_col=0)['radiant_won']

    #print(x.shape)
    #print(y.value_counts())

    columns_with_single_value = [col for col in x.columns if x[col].unique().shape[0] == 1]
    x = x.drop(columns_with_single_value, axis=1)

    x['radiant_gold'] = x['r1_gold'] + x['r2_gold'] + x['r3_gold'] + x['r4_gold'] + x['r5_gold']
    x['dire_gold'] = x['d1_gold'] + x['d2_gold'] + x['d3_gold'] + x['d4_gold'] + x['d5_gold']
    x['gold_diff'] = x['radiant_gold'] - x['dire_gold']
    x.drop(['radiant_gold', 'dire_gold'], axis=1, inplace=True)

    x['gold_diff'].hist()
    #plt.show()

    x_train, x_validation, y_train, y_validation = train_test_split(x, y, test_size=.33, random_state=1)
    x_train = x_train.fillna(0)
    x_validation = x_validation.fillna(0)

    scaler = MinMaxScaler()
    x_train = scaler.fit_transform(x_train)
    x_validation = scaler.fit_transform(x_validation)
    features = list(x.columns)

    param_grid = {'n_estimators': [70, 72, 74, 76, 78, 80, 82, 84, 86, 88, 90, 92, 94, 95, 96, 98, 100, 102, 104, 106, 108, 110], 'max_depth': [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]}


    clf = GridSearchCV(RandomForestClassifier(random_state=322), param_grid, verbose=3, n_jobs=-1)
    clf.fit(x_train, y_train)
    best_clf = clf.best_estimator_



    train_acc = accuracy_score(y_train, best_clf.predict(x_train))
    validation_acc = accuracy_score(y_validation, best_clf.predict(x_validation))
    lef.write(str(train_acc))
    lef.write(str(validation_acc))
    print('Train Accuracy:', train_acc)
    print('Validation Accuracy:', validation_acc)
    x = pd.read_csv('train.csv', index_col=0)
    y = pd.read_csv('target.csv', index_col=0)['radiant_won']

    # print(x.shape)
    # print(y.value_counts())

    columns_with_single_value = [col for col in x.columns if x[col].unique().shape[0] == 1]
    x = x.drop(columns_with_single_value, axis=1)

    x['radiant_gold'] = x['r1_gold'] + x['r2_gold'] + x['r3_gold'] + x['r4_gold'] + x['r5_gold']
    x['dire_gold'] = x['d1_gold'] + x['d2_gold'] + x['d3_gold'] + x['d4_gold'] + x['d5_gold']
    x['gold_diff'] = x['radiant_gold'] - x['dire_gold']
    x.drop(['radiant_gold', 'dire_gold'], axis=1, inplace=True)

    x['gold_diff'].hist()
    # plt.show()

    x_train, x_validation, y_train, y_validation = train_test_split(x, y, test_size=.33, random_state=1)
    x_train = x_train.fillna(0)
    x_validation = x_validation.fillna(0)

    scaler = MinMaxScaler()
    x_train = scaler.fit_transform(x_train)
    x_validation = scaler.fit_transform(x_validation)
    features = list(x.columns)

    n_es = [70, 72, 74, 76, 78, 80, 82, 84, 86, 88, 90, 92, 94, 95, 96, 98, 100, 102, 104, 106, 108, 110]
    max_d = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
    for i in range(len(n_es)):
        for j in range(len(max_d)):
            clf = RandomForestClassifier(n_estimators=n_es[i], max_depth=max_d[j], random_state=300)
            clf.fit(x_train, y_train)
            k = accuracy_score(y_train, clf.predict(x_train))
            l = accuracy_score(y_validation, clf.predict(x_validation))
            if k == train_acc:
                lef.write(str(n_es[i]))
                lef.write(str(max_d[j]))
                exit()

    #if hasattr(clf, 'coef_'):
    #    df_importances = sorted(list(zip(x.columns, clf.coef_.ravel())), key=lambda tpl: tpl[1], reverse=True)
    #else:
    #    df_importances= sorted(list(zip(x.columns, clf.feature_importances_.ravel())), key=lambda tpl: tpl[1], reverse=True)
    #df_importances = pd.DataFrame(df_importances, columns=['feature', 'importance'])
    #df_importances = df_importances.set_index('feature')
    #df_importances.plot(kind='bar', figsize=(15, 3))
    #print(display(df_importances.head()))
    #plt.show()
    """
    x_test = pd.read_csv('test.csv', index_col=0)
    y_submission = pd.read_csv('submission.csv', index_col=0)

    x_test['radiant_gold'] = x_test['r1_gold'] + x_test['r2_gold'] + x_test['r3_gold'] + x_test['r4_gold'] + x_test['r5_gold']
    x_test['dire_gold'] = x_test['d1_gold'] + x_test['d2_gold'] + x_test['d3_gold'] + x_test['d4_gold'] + x_test['d5_gold']
    x_test['gold_diff'] = x_test['radiant_gold'] - x_test['dire_gold']
    x_test.drop(['radiant_gold', 'dire_gold'], axis=1, inplace=True)

    x_test = x_test.fillna(0)
    x_test = x_test[features]

    scaler = MinMaxScaler()
    x_test = scaler.fit_transform(x_test)

    y_submission['radiant_won'] = clf.predict(x_test)

    current_timestamp = int(time.time())
    submission_path = 'submissions/{}.csv'.format(current_timestamp)

    if not os.path.exists('submissions'):
        os.makedirs('submissions')

    print(submission_path)
    y_submission.to_csv(submission_path, index=True)
    """