def rotate(A,B,C):
  return (B[0]-A[0])*(C[1]-B[1])-(B[1]-A[1])*(C[0]-B[0])

def grahamscan(A): # Алгоритм Грэхема
    D = []
    n = len(A) # число точек
    P = list(range(n)) # список номеров точек
    for i in range(1,n):
        if A[P[i]][0] < A[P[0]][0]: # если P[i]-ая точка лежит левее P[0]-ой точки
            P[i], P[0] = P[0], P[i] # меняем местами номера этих точек
    for i in range(2,n): # сортировка вставкой
        j = i
        while j > 1 and (rotate(A[P[0]],A[P[j-1]],A[P[j]]) < 0):
            P[j], P[j-1] = P[j-1], P[j]
            j -= 1
    S = [P[0],P[1]] # создаем стек
    for i in range(2,n):
        while rotate(A[S[-2]],A[S[-1]],A[P[i]]) < 0:
            S.pop() # pop(S)
        S.append(P[i]) # push(S,P[i])
    for i in range(len(S)):
        D.append(A[S[i]])
    return D
def peresechenie(x1_1, y1_1, x1_2, y1_2, x2_1, y2_1, x2_2, y2_2):
    # составляем формулы двух прямых
    A1 = y1_1 - y1_2
    B1 = x1_2 - x1_1
    C1 = x1_1 * y1_2 - x1_2 * y1_1
    A2 = y2_1 - y2_2
    B2 = x2_2 - x2_1
    C2 = x2_1 * y2_2 - x2_2 * y2_1
    if A1 == 0 and A2 == 0:
        if B1 == B2 and C1 == C2:
            return True
        else:
            return False
    elif A1 == 0 and B1 == 0:
        if A2 * y1_1 + B2 * x1_1 + C2 == 0:
            return False
        else:
            return True
    elif A1 == 0:
        y = C1 / B1
        x = (-C2 - B2 * y) / A2

    elif B1 * A2 - B2 * A1 != 0:
        y = (C2 * A1 - C1 * A2) / (B1 * A2 - B2 * A1)
        x = (-C1 - B1 * y) / A1
    # случай деления на ноль, то есть параллельность
    else:
        return False
    if min(x1_1, x1_2) <= x <= max(x1_1, x1_2) and min(y1_1, y1_2) <= y <= max(y1_1, y1_2) and \
                            min(x2_1, x2_2) <= x <= max(x2_1, x2_2) and min(y2_1, y2_2) <= y <= max(y2_1, y2_2):
        return True
    else:
        return False


n = int(input())
Krasivo = []
Urod = []
for i in range(n):
    K = list(map(int, input().split()))
    if K[2] == 1:
        Krasivo.append([K[0], K[1]])
    else:
        Urod.append([K[0], K[1]])
#print(Krasivo)
#print(Urod)
if len(Krasivo) > 2:
    K_MVO = grahamscan(Krasivo)
else:
    K_MVO = Krasivo

if len(Urod) > 2:
    U_MVO = grahamscan(Urod)
else:
    U_MVO = Urod
if len(Urod) == 0 or len(Krasivo) == 0:
    print('Yes')
    exit()
Flag = True
for i in range(len(U_MVO)):
    for j in range(len(K_MVO)):
        if peresechenie(U_MVO[i][0],U_MVO[i][1], U_MVO[(i+1) % len(U_MVO)][0],U_MVO[(i+1) % len(U_MVO)][1], K_MVO[j][0],K_MVO[j][1], K_MVO[(j+1) % len(K_MVO)][0],K_MVO[(j+1) % len(K_MVO)][1]):
            Flag = False

J = K_MVO + [U_MVO[0]]
if len(J) > 2:
    if grahamscan(J) == grahamscan(K_MVO) and Flag:
        Flag = False
J = U_MVO + [K_MVO[0]]
if len(J) > 2:
    if grahamscan(J) == grahamscan(U_MVO) and Flag:
        Flag = False

if Flag:
    print('Yes')
else:
    print('No')