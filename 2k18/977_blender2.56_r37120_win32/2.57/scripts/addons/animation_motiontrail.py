# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####


bl_info = {
    'name': 'Motion Trail',
    'author': 'Bart Crouch',
    'version': (2, 0, 1),
    'blender': (2, 5, 6),
    'api': 35272,
    'location': 'View3D > Toolbar > Motion Trail tab',
    'warning': '',
    'description': 'Display and edit motion trails in the 3d-view',
    'wiki_url': 'http://wiki.blender.org/index.php/Extensions:2.5/Py/Scripts/Animation/Motion_Trail',
    'tracker_url': 'http://projects.blender.org/tracker/index.php?func=detail&aid=26374',
    'category': 'Animation'}


import bgl
import blf
import bpy
import math
import mathutils


# dehomonogize a vector
def dehomonogize(vector):
    return(mathutils.Vector([vector[0] / vector[3], vector[1] / vector[3], vector[2] / vector[3]]))


# turn screen coordinates (x,y) into world coordinates vector
def screen_to_world(context, x, y):
    # get screen information
    mid_x = context.region.width / 2
    mid_y = context.region.height / 2
    width = context.region.width
    height = context.region.height
    inverse_view_mat = context.space_data.region_3d.perspective_matrix.copy().inverted()
    
    dx = 2 * (x - mid_x) / width
    dy = 2 * (y - mid_y) / height
    vector = mathutils.Vector([dx, dy, 0, 1])
    vector *= inverse_view_mat
    vector = dehomonogize(vector)
    
    return(vector)


# turn 3d world coordinates vector into screen coordinate integers (x,y)
def world_to_screen(context, vector):
    # get screen information
    mid_x = context.region.width / 2
    mid_y = context.region.height / 2
    width = context.region.width
    height = context.region.height
    view_mat = context.space_data.region_3d.perspective_matrix
    
    vector = vector.copy().to_4d() * view_mat
    vector = dehomonogize(vector)
    x = int(mid_x + vector[0]*width/2)
    y = int(mid_y + vector[1]*height/2)
    
    return(x, y)


# get position of keyframes and handles at the start of dragging
def get_original_animation_data(context, keyframes):
    keyframes_ori = {}
    handles_ori = {}
    
    for object in context.selected_objects:
        if not object.animation_data:
            continue
        curves = [fc for fc in object.animation_data.action.fcurves if fc.data_path == 'location']
        if len(curves) == 0:
            continue
        objectname = object.name
        fcx, fcy, fcz = curves
        
        # get keyframe positions
        keyframes_ori[objectname] = {}
        for frame in keyframes[objectname]:
            locx = fcx.evaluate(frame)
            locy = fcy.evaluate(frame)
            locz = fcz.evaluate(frame)
            keyframes_ori[objectname][frame] = [frame, [locx, locy, locz]]
        
        # get handle positions
        handles_ori[objectname] = {}
        for frame in keyframes[objectname]:
            handles_ori[objectname][frame] = {}
            for kf in fcx.keyframe_points:
                if kf.co[0] == frame:
                    left_x = kf.handle_left[:]
                    right_x = kf.handle_right[:]
                    break
            for kf in fcy.keyframe_points:
                if kf.co[0] == frame:
                    left_y = kf.handle_left[:]
                    right_y = kf.handle_right[:]
                    break
            for kf in fcz.keyframe_points:
                if kf.co[0] == frame:
                    left_z = kf.handle_left[:]
                    right_z = kf.handle_right[:]
                    break
            handles_ori[objectname][frame]["left"] = [left_x, left_y, left_z]
            handles_ori[objectname][frame]["right"] = [right_x, right_y, right_z]
    
    return(keyframes_ori, handles_ori)


# callback function that calculates positions of all things that need be drawn
def calc_callback(self, context):
    self.paths = {} # key: objectname, value: list of lists with x, y, colour
    self.keyframes = {} # key: objectname, value: dict with frame as key and [x,y] as value
    self.handles = {} # key: objectname, value: dict of dicts
    self.timebeads = {} # key: objectname, value: dict with frame as key and [x,y] as value
    self.click = {} # key: objectname, value: list of lists with frame, type, loc-vector
    
    for object in context.selected_objects:
        if not object.animation_data:
            continue
        # get curves
        curves = [fc for fc in object.animation_data.action.fcurves if fc.data_path == 'location']
        if len(curves) == 0:
            continue
        ranges = [val for c in curves for val in c.range()]
        ranges.sort()
        range_min = round(ranges[0])
        range_max = round(ranges[-1])
        fcx, fcy, fcz = curves
        
        # get location data of motion path
        path = []
        locx = fcx.evaluate(range_min - 1)
        locy = fcy.evaluate(range_min - 1)
        locz = fcz.evaluate(range_min - 1)
        prev_loc = mathutils.Vector([locx, locy, locz])
        speeds = []
        
        for frame in range(range_min, range_max + 1):
            locx = fcx.evaluate(frame)
            locy = fcy.evaluate(frame)
            locz = fcz.evaluate(frame)
            loc = mathutils.Vector([locx, locy, locz])
            x, y = world_to_screen(context, loc)
            if context.window_manager.motion_trail.path_style == 'simple':
                path.append([x, y, [0.0, 0.0, 0.0], frame])
            else:
                dloc = (loc - prev_loc).length
                path.append([x, y, dloc, frame])
                speeds.append(dloc)
                prev_loc = loc
        # calculate colour of path
        if context.window_manager.motion_trail.path_style == 'speed':
            speeds.sort()
            min_speed = speeds[0]
            d_speed = speeds[-1] - min_speed
            for i, [x, y, d_loc, frame] in enumerate(path):
                relative_speed = (d_loc - min_speed) / d_speed # values from 0.0 to 1.0
                red = min(1.0, 2.0 * relative_speed)
                blue = min(1.0, 2.0 - (2.0 * relative_speed))
                path[i][2] = [red, 0.0, blue]
        elif context.window_manager.motion_trail.path_style == 'acceleration':
            accelerations = []
            prev_speed = 0.0
            for i, [x, y, d_loc, frame] in enumerate(path):
                accel = d_loc - prev_speed
                accelerations.append(accel)
                path[i][2] = accel
                prev_speed = d_loc
            accelerations.sort()
            min_accel = accelerations[0]
            max_accel = accelerations[-1]
            for i, [x, y, accel, frame] in enumerate(path):
                if accel < 0:
                    relative_accel = accel / min_accel # values from 0.0 to 1.0
                    green = 1.0 - relative_accel
                    path[i][2] = [1.0, green, 0.0]
                elif accel > 0:
                    relative_accel = accel / max_accel # values from 0.0 to 1.0
                    red = 1.0 - relative_accel
                    path[i][2] = [red, 1.0, 0.0]
                else:
                    path[i][2] = [1.0, 1.0, 0.0]
        self.paths[object.name] = path
        
        # get keyframes and handles
        keyframes = {}
        handle_difs = {}
        kf_time = []
        click = []
        for fc in curves:
            for kf in fc.keyframe_points:
                # handles
                if kf.co[0] not in handle_difs:
                    handle_difs[kf.co[0]] = {"left":mathutils.Vector(), "right":mathutils.Vector()}
                handle_difs[kf.co[0]]["left"][fc.array_index] = (mathutils.Vector(kf.handle_left[:]) - mathutils.Vector(kf.co[:])).normalized()[1]
                handle_difs[kf.co[0]]["right"][fc.array_index] = (mathutils.Vector(kf.handle_right[:]) - mathutils.Vector(kf.co[:])).normalized()[1]
                # keyframes
                if kf.co[0] in kf_time:
                    continue
                kf_time.append(kf.co[0])
                co = kf.co[0]
                locx = fcx.evaluate(co)
                locy = fcy.evaluate(co)
                locz = fcz.evaluate(co)
                x, y = world_to_screen(context, mathutils.Vector([locx, locy, locz]))
                keyframes[kf.co[0]] = [x, y]
                if context.window_manager.motion_trail.mode != 'speed':
                    # can't select keyframes in speed mode
                    click.append([kf.co[0], "keyframe", mathutils.Vector([x,y])])
        self.keyframes[object.name] = keyframes
        
        # handles are only shown in location-altering mode
        if context.window_manager.motion_trail.mode == 'location':
            # calculate handle positions
            handles = {}
            for frame, vecs in handle_difs.items():
                x = fcx.evaluate(frame)
                y = fcy.evaluate(frame)
                z = fcz.evaluate(frame)
                vec_keyframe = mathutils.Vector([x, y, z])
                x_left, y_left = world_to_screen(context, vecs["left"]*2 + vec_keyframe)
                x_right, y_right = world_to_screen(context, vecs["right"]*2 + vec_keyframe)
                handles[frame] = {"left":[x_left, y_left], "right":[x_right, y_right]}
                click.append([frame, "handle_left", mathutils.Vector([x_left, y_left])])
                click.append([frame, "handle_right", mathutils.Vector([x_right, y_right])])
            self.handles[object.name] = handles
        
        # calculate timebeads for timing mode
        if context.window_manager.motion_trail.mode == 'timing':
            timebeads = {}
            n = context.window_manager.motion_trail.timebeads * (len(kf_time) - 1)
            dframe = (range_max - range_min) / (n + 1)
            for i in range(1, n+1):
                frame = range_min + i * dframe
                locx = fcx.evaluate(frame)
                locy = fcy.evaluate(frame)
                locz = fcz.evaluate(frame)
                x, y = world_to_screen(context, mathutils.Vector([locx, locy, locz]))
                timebeads[frame] = [x, y]
                click.append([frame, "timebead", mathutils.Vector([x,y])])
            self.timebeads[object.name] = timebeads
        
        # calculate timebeads for speed mode
        if context.window_manager.motion_trail.mode == 'speed':
            angles = dict([[kf, {"left":[], "right":[]}] for kf in self.keyframes[object.name]])
            for fc in curves:
                for i, kf in enumerate(fc.keyframe_points):
                    if i != 0:
                        angle = mathutils.Vector([-1, 0]).angle(mathutils.Vector(kf.handle_left) - mathutils.Vector(kf.co), 0)
                        if angle != 0:
                            angles[kf.co[0]]["left"].append(angle)
                    if i != len(fc.keyframe_points) - 1:
                        angle = mathutils.Vector([1, 0]).angle(mathutils.Vector(kf.handle_right) - mathutils.Vector(kf.co), 0)
                        if angle != 0:
                            angles[kf.co[0]]["right"].append(angle)
            timebeads = {}
            kf_time.sort()
            for frame, sides in angles.items():
                if sides["left"]:
                    perc = (sum(sides["left"]) / len(sides["left"])) / (math.pi / 2)
                    perc = max(0.4, min(1, perc * 5))
                    previous = kf_time[kf_time.index(frame) - 1]
                    bead_frame = frame - perc * ((frame - previous - 2) / 2)
                    locx = fcx.evaluate(bead_frame)
                    locy = fcy.evaluate(bead_frame)
                    locz = fcz.evaluate(bead_frame)
                    x, y = world_to_screen(context, mathutils.Vector([locx, locy, locz]))
                    timebeads[bead_frame] = [x, y]
                    click.append([bead_frame, "timebead", mathutils.Vector([x,y])])
                if sides["right"]:
                    perc = (sum(sides["right"]) / len(sides["right"])) / (math.pi / 2)
                    perc = max(0.4, min(1, perc * 5))
                    next = kf_time[kf_time.index(frame) + 1]
                    bead_frame = frame + perc * ((next - frame - 2) / 2)
                    locx = fcx.evaluate(bead_frame)
                    locy = fcy.evaluate(bead_frame)
                    locz = fcz.evaluate(bead_frame)
                    x, y = world_to_screen(context, mathutils.Vector([locx, locy, locz]))
                    timebeads[bead_frame] = [x, y]
                    click.append([bead_frame, "timebead", mathutils.Vector([x,y])])
            self.timebeads[object.name] = timebeads
        
        self.click[object.name] = click


# draw in 3d-view
def draw_callback(self, context):
    # polling
    if context.mode != 'OBJECT' or context.window_manager.motion_trail.enabled != 1:
        return
    
    # display limits
    if context.window_manager.motion_trail.path_before != 0:
        limit_min = context.scene.frame_current - context.window_manager.motion_trail.path_before
    else:
        limit_min = -1e6
    if context.window_manager.motion_trail.path_after != 0:
        limit_max = context.scene.frame_current + context.window_manager.motion_trail.path_after
    else:
        limit_max = 1e6
    
    # draw motion path
    bgl.glEnable(bgl.GL_BLEND)
    bgl.glLineWidth(context.window_manager.motion_trail.path_width)
    alpha = 1.0 - (context.window_manager.motion_trail.path_transparency / 100.0)
    if context.window_manager.motion_trail.path_style == 'simple':
        bgl.glColor4f(0.0, 0.0, 0.0, alpha)
        for objectname, path in self.paths.items():
            bgl.glBegin(bgl.GL_LINE_STRIP)
            for x, y, colour, frame in path:
                if frame < limit_min or frame > limit_max:
                    continue
                bgl.glVertex2i(x, y)
            bgl.glEnd()
    else:
        for objectname, path in self.paths.items():
            for i, [x, y, colour, frame] in enumerate(path):
                if frame < limit_min or frame > limit_max:
                    continue
                r, g, b = colour
                if i != 0:
                    prev_path = path[i-1]
                    halfway = [(x + prev_path[0])/2, (y + prev_path[1])/2]
                    bgl.glColor4f(r, g, b, alpha)
                    bgl.glBegin(bgl.GL_LINE_STRIP)
                    bgl.glVertex2i(int(halfway[0]), int(halfway[1]))
                    bgl.glVertex2i(x, y)
                    bgl.glEnd()
                if i != len(path) - 1:
                    next_path = path[i+1]
                    halfway = [(x + next_path[0])/2, (y + next_path[1])/2]
                    bgl.glColor4f(r, g, b, alpha)
                    bgl.glBegin(bgl.GL_LINE_STRIP)
                    bgl.glVertex2i(int(halfway[0]), int(halfway[1]))
                    bgl.glVertex2i(x, y)
                    bgl.glEnd()
    
    # time beads are shown in speed and timing modes
    if context.window_manager.motion_trail.mode in ['speed', 'timing']:
        bgl.glColor4f(0.0, 1.0, 0.0, 1.0)
        bgl.glPointSize(4)
        bgl.glBegin(bgl.GL_POINTS)
        for objectname, values in self.timebeads.items():
            for frame, coords in values.items():
                if frame < limit_min or frame > limit_max:
                    continue
                if self.active_timebead and objectname == self.active_timebead[0] and abs(frame - self.active_timebead[1]) < 1e-4:
                    bgl.glEnd()
                    bgl.glColor4f(1.0, 0.5, 0.0, 1.0)
                    bgl.glBegin(bgl.GL_POINTS)
                    bgl.glVertex2i(coords[0], coords[1])
                    bgl.glEnd()
                    bgl.glColor4f(0.0, 1.0, 0.0, 1.0)
                    bgl.glBegin(bgl.GL_POINTS)
                else:
                    bgl.glVertex2i(coords[0], coords[1])
        bgl.glEnd()
    
    # handles are only shown in location mode
    if context.window_manager.motion_trail.mode == 'location':
        # draw handle-lines
        bgl.glColor4f(0.0, 0.0, 0.0, 1.0)
        bgl.glLineWidth(1)
        bgl.glBegin(bgl.GL_LINES)
        for objectname, values in self.handles.items():
            for frame, sides in values.items():
                if frame < limit_min or frame > limit_max:
                    continue
                for side, coords in sides.items():
                    if self.active_handle and objectname == self.active_handle[0] and side == self.active_handle[2] and abs(frame - self.active_handle[1]) < 1e-4:
                        bgl.glEnd()
                        bgl.glColor4f(.75, 0.25, 0.0, 1.0)
                        bgl.glBegin(bgl.GL_LINES)
                        bgl.glVertex2i(self.keyframes[objectname][frame][0], self.keyframes[objectname][frame][1])
                        bgl.glVertex2i(coords[0], coords[1])
                        bgl.glEnd()
                        bgl.glColor4f(0.0, 0.0, 0.0, 1.0)
                        bgl.glBegin(bgl.GL_LINES)
                    else:
                        bgl.glVertex2i(self.keyframes[objectname][frame][0], self.keyframes[objectname][frame][1])
                        bgl.glVertex2i(coords[0], coords[1])
        bgl.glEnd()
        
        # draw handles
        bgl.glColor4f(1.0, 1.0, 0.0, 1.0)
        bgl.glPointSize(4)
        bgl.glBegin(bgl.GL_POINTS)
        for objectname, values in self.handles.items():
            for frame, sides in values.items():
                if frame < limit_min or frame > limit_max:
                    continue
                for side, coords in sides.items():
                    if self.active_handle and objectname == self.active_handle[0] and side == self.active_handle[2] and abs(frame - self.active_handle[1]) < 1e-4:
                        bgl.glEnd()
                        bgl.glColor4f(1.0, 0.5, 0.0, 1.0)
                        bgl.glBegin(bgl.GL_POINTS)
                        bgl.glVertex2i(coords[0], coords[1])
                        bgl.glEnd()
                        bgl.glColor4f(1.0, 1.0, 0.0, 1.0)
                        bgl.glBegin(bgl.GL_POINTS)
                    else:
                        bgl.glVertex2i(coords[0], coords[1])
        bgl.glEnd()
        
    # draw keyframes
    bgl.glColor4f(1.0, 1.0, 0.0, 1.0)
    bgl.glPointSize(6)
    bgl.glBegin(bgl.GL_POINTS)
    for objectname, values in self.keyframes.items():
        for frame, coords in values.items():
            if frame < limit_min or frame > limit_max:
                continue
            if self.active_keyframe and objectname == self.active_keyframe[0] and abs(frame - self.active_keyframe[1]) < 1e-4:
                bgl.glEnd()
                bgl.glColor4f(1.0, 0.5, 0.0, 1.0)
                bgl.glBegin(bgl.GL_POINTS)
                bgl.glVertex2i(coords[0], coords[1])
                bgl.glEnd()
                bgl.glColor4f(1.0, 1.0, 0.0, 1.0)
                bgl.glBegin(bgl.GL_POINTS)
            else:
                bgl.glVertex2i(coords[0], coords[1])
    bgl.glEnd()
    
    # draw keyframe-numbers
    if context.window_manager.motion_trail.keyframe_numbers:
        blf.size(0, 12, 72)
        bgl.glColor4f(1.0, 1.0, 0.0, 1.0)
        for objectname, values in self.keyframes.items():
            for frame, coords in values.items():
                if frame < limit_min or frame > limit_max:
                    continue
                blf.position(0, coords[0] + 3, coords[1] + 3, 0)
                text = str(frame).split(".")
                if len(text) == 1:
                    text = text[0]
                elif len(text[1]) == 1 and text[1] == "0":
                    text = text[0]
                else:
                    text = text[0] + "." + text[1][0]
                if self.active_keyframe and objectname == self.active_keyframe[0] and abs(frame - self.active_keyframe[1]) < 1e-4:
                    bgl.glColor4f(1.0, 0.5, 0.0, 1.0)
                    blf.draw(0, text)
                    bgl.glColor4f(1.0, 1.0, 0.0, 1.0)
                else:
                    blf.draw(0, text)
    
    # restore opengl defaults
    bgl.glLineWidth(1)
    bgl.glDisable(bgl.GL_BLEND)
    bgl.glColor4f(0.0, 0.0, 0.0, 1.0)


# change data based on mouse movement
def drag(context, event, drag_mouse_ori, active_keyframe, active_handle, active_timebead, keyframes_ori, handles_ori):
    # change 3d-location of keyframe
    if context.window_manager.motion_trail.mode == 'location' and active_keyframe:
        mouse_ori_world = screen_to_world(context, drag_mouse_ori[0], drag_mouse_ori[1])
        vector = screen_to_world(context, event.mouse_region_x, event.mouse_region_y)
        d = vector - mouse_ori_world
        objectname, frame, frame_ori = active_keyframe
        loc_ori = keyframes_ori[objectname][frame][1]
        new_loc = mathutils.Vector(loc_ori) + d
        curves = [fc for fc in bpy.data.objects[objectname].animation_data.action.fcurves if fc.data_path == 'location']
        
        for i, curve in enumerate(curves):
            for kf in curve.keyframe_points:
                if kf.co[0] == frame:
                    kf.co[1] = new_loc[i]
                    kf.handle_left[1] = handles_ori[objectname][frame]["left"][i][1] + d[i]
                    kf.handle_right[1] = handles_ori[objectname][frame]["right"][i][1] + d[i]
                    break
    
    # change 3d-location of handle
    elif context.window_manager.motion_trail.mode == 'location' and active_handle:
        mouse_ori_world = screen_to_world(context, drag_mouse_ori[0], drag_mouse_ori[1])
        vector = screen_to_world(context, event.mouse_region_x, event.mouse_region_y)
        d = vector - mouse_ori_world
        objectname, frame, side = active_handle
        curves = [fc for fc in bpy.data.objects[objectname].animation_data.action.fcurves if fc.data_path == 'location']
        
        for i, curve in enumerate(curves):
            for kf in curve.keyframe_points:
                if kf.co[0] == frame:
                    if side == "left":
                        kf.handle_left[1] = handles_ori[objectname][frame]["left"][i][1] + d[i]
                        if kf.handle_left_type in ['ALIGNED', 'ANIM_CLAMPED', 'AUTO']:
                            dif = (abs(handles_ori[objectname][frame]["right"][i][0] - kf.co[0]) / abs(kf.handle_left[0] - kf.co[0])) * d[i]
                            kf.handle_right[1] = handles_ori[objectname][frame]["right"][i][1] - dif
                    elif side == "right":
                        kf.handle_right[1] = handles_ori[objectname][frame]["right"][i][1] + d[i]
                        if kf.handle_right_type in ['ALIGNED', 'ANIM_CLAMPED', 'AUTO']:
                            dif = (abs(handles_ori[objectname][frame]["left"][i][0] - kf.co[0]) / abs(kf.handle_right[0] - kf.co[0])) * d[i]
                            kf.handle_left[1] = handles_ori[objectname][frame]["left"][i][1] - dif
                    break
    
    # change position of all keyframes on timeline
    elif context.window_manager.motion_trail.mode == 'timing' and active_timebead:
        objectname, frame, frame_ori = active_timebead
        curves = [fc for fc in bpy.data.objects[objectname].animation_data.action.fcurves if fc.data_path == 'location']
        ranges = [val for c in curves for val in c.range()]
        ranges.sort()
        range_min = round(ranges[0])
        range_max = round(ranges[-1])
        range = range_max - range_min
        dx_screen = -(mathutils.Vector([event.mouse_region_x, event.mouse_region_y]) - drag_mouse_ori)[0]
        dx_screen = dx_screen / context.region.width * range
        new_frame = frame + dx_screen
        shift_low = max(1e-4, (new_frame - range_min) / (frame - range_min))
        shift_high = max(1e-4, (range_max - new_frame) / (range_max - frame))
        
        new_mapping = {}
        for i, curve in enumerate(curves):
            for j, kf in enumerate(curve.keyframe_points):
                frame_map = kf.co[0]
                if frame_map < range_min + 1e-4 or frame_map > range_max - 1e-4:
                    continue
                frame_ori = False
                for f in keyframes_ori[objectname]:
                    if abs(f - frame_map) < 1e-4:
                        frame_ori = keyframes_ori[objectname][f][0]
                        value_ori = keyframes_ori[objectname][f]
                        break
                if not frame_ori:
                    continue
                if frame_ori <= frame:
                    frame_new = (frame_ori - range_min) * shift_low + range_min
                else:
                    frame_new = range_max - (range_max - frame_ori) * shift_high
                frame_new = max(range_min + j, min(frame_new, range_max - (len(curve.keyframe_points)-j)+1))
                d_frame = frame_new - frame_ori
                if frame_new not in new_mapping:
                    new_mapping[frame_new] = value_ori
                kf.co[0] = frame_new
                kf.handle_left[0] = handles_ori[objectname][frame_ori]["left"][i][0] + d_frame
                kf.handle_right[0] = handles_ori[objectname][frame_ori]["right"][i][0] + d_frame
        del keyframes_ori[objectname]
        keyframes_ori[objectname] = {}
        for new_frame, value in new_mapping.items():
            keyframes_ori[objectname][new_frame] = value
    
    # change position of active keyframe on the timeline
    elif context.window_manager.motion_trail.mode == 'timing' and active_keyframe:
        objectname, frame, frame_ori = active_keyframe
        # detect direction of movement
        mouse_ori_world = screen_to_world(context, drag_mouse_ori[0], drag_mouse_ori[1])
        vector = screen_to_world(context, event.mouse_region_x, event.mouse_region_y)
        d = vector - mouse_ori_world
        locs_ori = [[f_ori, coords] for f_mapped, [f_ori, coords] in keyframes_ori[objectname].items()]
        locs_ori.sort()
        direction = 1
        range = False
        for i, [f_ori, coords] in enumerate(locs_ori):
            if abs(frame_ori - f_ori) < 1e-4:
                if i == 0:
                    # first keyframe, nothing before it
                    direction = -1
                    range = [f_ori, locs_ori[i+1][0]]
                elif i == len(locs_ori) - 1:
                    # last keyframe, nothing after it
                    range = [locs_ori[i-1][0], f_ori]
                else:
                    current = mathutils.Vector(coords)
                    next = mathutils.Vector(locs_ori[i+1][1])
                    previous = mathutils.Vector(locs_ori[i-1][1])
                    angle_to_next = d.angle(next - current, 0)
                    angle_to_previous = d.angle(previous-current, 0)
                    if angle_to_previous < angle_to_next:
                        # mouse movement is in direction of previous keyframe
                        direction = -1
                    range = [locs_ori[i-1][0], locs_ori[i+1][0]]
                break
        direction *= -1 # feels more natural in 3d-view
        if not range:
            # keyframe not found, should be impossible, but better safe than sorry
            return(active_keyframe, active_timebead, keyframes_ori)
        # calculate strength of movement
        d_screen = mathutils.Vector([event.mouse_region_x, event.mouse_region_y]) - drag_mouse_ori
        if d_screen.length != 0:
            d_screen = d_screen.length / (abs(d_screen[0])/d_screen.length*context.region.width + abs(d_screen[1])/d_screen.length*context.region.height)
            d_screen *= direction  # d_screen value ranges from -1.0 to 1.0
        else:
            d_screen = 0.0
        new_frame = d_screen * (range[1] - range[0]) + frame_ori
        max_frame = range[1]
        if max_frame == frame_ori:
            max_frame += 1
        min_frame = range[0]
        if min_frame == frame_ori:
            min_frame -= 1
        new_frame = min(max_frame - 1, max(min_frame + 1, new_frame))
        d_frame = new_frame - frame_ori
        curves = [fc for fc in bpy.data.objects[objectname].animation_data.action.fcurves if fc.data_path == 'location']
        
        for i, curve in enumerate(curves):
            for kf in curve.keyframe_points:
                if abs(kf.co[0] - frame) < 1e-4:
                    kf.co[0] = new_frame
                    kf.handle_left[0] = handles_ori[objectname][frame_ori]["left"][i][0] + d_frame
                    kf.handle_right[0] = handles_ori[objectname][frame_ori]["right"][i][0] + d_frame
                    break
        active_keyframe = [objectname, new_frame, frame_ori]
    
    # change position of active timebead on the timeline, thus altering the speed
    elif context.window_manager.motion_trail.mode == 'speed' and active_timebead:
        objectname, frame, frame_ori = active_timebead
        # detect direction of movement
        mouse_ori_world = screen_to_world(context, drag_mouse_ori[0], drag_mouse_ori[1])
        vector = screen_to_world(context, event.mouse_region_x, event.mouse_region_y)
        d = vector - mouse_ori_world
        curves = [fc for fc in bpy.data.objects[objectname].animation_data.action.fcurves if fc.data_path == 'location']
        fcx, fcy, fcz = curves
        locx = fcx.evaluate(frame_ori)
        locy = fcy.evaluate(frame_ori)
        locz = fcz.evaluate(frame_ori)
        loc_ori = mathutils.Vector([locx, locy, locz])
        keyframes = [kf for kf in keyframes_ori[objectname]]
        keyframes.append(frame_ori)
        keyframes.sort()
        frame_index = keyframes.index(frame_ori)
        kf_prev = keyframes[frame_index - 1]
        kf_next = keyframes[frame_index + 1]
        loc_prev = mathutils.Vector(keyframes_ori[objectname][kf_prev][1])
        loc_next = mathutils.Vector(keyframes_ori[objectname][kf_next][1])
        angle_to_next = d.angle(loc_next - loc_ori, 0)
        angle_to_previous = d.angle(loc_prev - loc_ori, 0)
        if angle_to_previous < angle_to_next:
            direction = -1
        else:
            direction = 1
        direction *= -1 # feels more natural in 3d-view
        if (kf_next - frame_ori) < (frame_ori - kf_prev):
            kf_bead = kf_next
            side = "left"
        else:
            kf_bead = kf_prev
            side = "right"
        
        # calculate strength of movement
        d_screen = mathutils.Vector([event.mouse_region_x, event.mouse_region_y]) - drag_mouse_ori
        if d_screen.length != 0:
            d_screen = d_screen.length / (abs(d_screen[0])/d_screen.length*context.region.width + abs(d_screen[1])/d_screen.length*context.region.height)
            d_screen *= direction  # d_screen value ranges from -1.0 to 1.0
        else:
            d_screen = 0.0
        d_frame = d_screen * (kf_next - kf_prev)
        
        angles = []
        for i, curve in enumerate(curves):
            for kf in curve.keyframe_points:
                if abs(kf.co[0] - kf_bead) < 1e-4:
                    if side == "left":
                        # left side
                        kf.handle_left[0] = min(handles_ori[objectname][kf_bead]["left"][i][0] + d_frame, kf_bead - 1)
                        angle = mathutils.Vector([-1, 0]).angle(mathutils.Vector(kf.handle_left) - mathutils.Vector(kf.co), 0)
                        if angle != 0:
                            angles.append(angle)
                    else:
                        # right side
                        kf.handle_right[0] = max(handles_ori[objectname][kf_bead]["right"][i][0] + d_frame, kf_bead + 1)
                        angle = mathutils.Vector([1, 0]).angle(mathutils.Vector(kf.handle_right) - mathutils.Vector(kf.co), 0)
                        if angle != 0:
                            angles.append(angle)
                    break
        
        # update frame of active_timebead
        perc = (sum(angles) / len(angles)) / (math.pi / 2)
        perc = max(0.4, min(1, perc * 5))
        if side == "left":
            bead_frame = kf_bead - perc * ((kf_bead - kf_prev - 2) / 2)
        else:
            bead_frame = kf_bead + perc * ((kf_next - kf_bead - 2) / 2)
        active_timebead = [objectname, bead_frame, frame_ori]
    
    return(active_keyframe, active_timebead, keyframes_ori)


# revert changes made by dragging
def cancel_drag(context, active_keyframe, active_handle, active_timebead, keyframes_ori, handles_ori):
    # revert change in 3d-location of active keyframe and its handles
    if context.window_manager.motion_trail.mode == 'location' and active_keyframe:
        objectname, frame, frame_ori = active_keyframe
        curves = [fc for fc in bpy.data.objects[objectname].animation_data.action.fcurves if fc.data_path == 'location']
        for i, curve in enumerate(curves):
            for kf in curve.keyframe_points:
                if kf.co[0] == frame:
                    kf.co[1] = keyframes_ori[objectname][frame][1][i]
                    kf.handle_left[1] = handles_ori[objectname][frame]["left"][i][1]
                    kf.handle_right[1] = handles_ori[objectname][frame]["right"][i][1]
                    break
    
    # revert change in 3d-location of active handle
    elif context.window_manager.motion_trail.mode == 'location' and active_handle:
        objectname, frame, side = active_handle
        curves = [fc for fc in bpy.data.objects[objectname].animation_data.action.fcurves if fc.data_path == 'location']
        for i, curve in enumerate(curves):
            for kf in curve.keyframe_points:
                if kf.co[0] == frame:
                    kf.handle_left[1] = handles_ori[objectname][frame]["left"][i][1]
                    kf.handle_right[1] = handles_ori[objectname][frame]["right"][i][1]
                    break
    
    # revert position of all keyframes and handles on timeline
    elif context.window_manager.motion_trail.mode == 'timing' and active_timebead:
        objectname, frame, frame_ori = active_timebead
        curves = [fc for fc in bpy.data.objects[objectname].animation_data.action.fcurves if fc.data_path == 'location']
        for i, curve in enumerate(curves):
            for kf in curve.keyframe_points:
                for kf_ori, [frame_ori, loc] in keyframes_ori[objectname].items():
                    if abs(kf.co[0] - kf_ori) < 1e-4:
                        kf.co[0] = frame_ori
                        kf.handle_left[0] = handles_ori[objectname][frame_ori]["left"][i][0]
                        kf.handle_right[0] = handles_ori[objectname][frame_ori]["right"][i][0]
                        break
    
    # revert position of active keyframe and its handles on the timeline
    elif context.window_manager.motion_trail.mode == 'timing' and active_keyframe:
        objectname, frame, frame_ori = active_keyframe
        curves = [fc for fc in bpy.data.objects[objectname].animation_data.action.fcurves if fc.data_path == 'location']
        for i, curve in enumerate(curves):
            for kf in curve.keyframe_points:
                if abs(kf.co[0] - frame) < 1e-4:
                    kf.co[0] = keyframes_ori[objectname][frame_ori][0]
                    kf.handle_left[0] = handles_ori[objectname][frame_ori]["left"][i][0]
                    kf.handle_right[0] = handles_ori[objectname][frame_ori]["right"][i][0]
                    break
        active_keyframe = [objectname, frame_ori, frame_ori]
    
    # revert position of handles on the timeline
    elif context.window_manager.motion_trail.mode == 'speed' and active_timebead:
        objectname, frame, frame_ori = active_timebead
        curves = [fc for fc in bpy.data.objects[objectname].animation_data.action.fcurves if fc.data_path == 'location']
        keyframes = [kf for kf in keyframes_ori[objectname]]
        keyframes.append(frame_ori)
        keyframes.sort()
        frame_index = keyframes.index(frame_ori)
        kf_prev = keyframes[frame_index - 1]
        kf_next = keyframes[frame_index + 1]
        if (kf_next - frame_ori) < (frame_ori - kf_prev):
            kf_frame = kf_next
        else:
            kf_frame = kf_prev
        for i, curve in enumerate(curves):
            for kf in curve.keyframe_points:
                if kf.co[0] == kf_frame:
                    kf.handle_left[0] = handles_ori[objectname][kf_frame]["left"][i][0]
                    kf.handle_right[0] = handles_ori[objectname][kf_frame]["right"][i][0]
                    break
        active_timebead = [objectname, frame_ori, frame_ori]
    
    return(active_keyframe, active_timebead)


class MotionTrailOperator(bpy.types.Operator):
    '''Edit motion trails in 3d-view'''
    bl_idname = "view3d.motion_trail"
    bl_label = "Motion Trail"
    
    def modal(self, context, event):
        if context.window_manager.motion_trail.enabled == -1:
            context.window_manager.motion_trail.enabled = 0
            context.region.callback_remove(self.handle1)
            context.region.callback_remove(self.handle2)
            context.area.tag_redraw()
            return {'FINISHED'}
        
        select = context.user_preferences.inputs.select_mouse
        
        if event.type == 'Q' and (self.active_keyframe or self.active_handle or self.active_timebead) and event.value == 'PRESS':
            if not self.drag:
                # start drag
                self.keyframes_ori, self.handles_ori = get_original_animation_data(context, self.keyframes)
                self.drag_mouse_ori = mathutils.Vector([event.mouse_region_x, event.mouse_region_y])
                self.drag = True
            else:
                # stop drag
                self.drag = False
        elif event.type == 'ESC' and self.drag and event.value == 'PRESS':
            # cancel drag
            self.drag = False
            self.active_keyframe, self.active_timebead = cancel_drag(context, self.active_keyframe, self.active_handle, self.active_timebead, self.keyframes_ori, self.handles_ori)
        elif event.type == 'MOUSEMOVE' and self.drag:
            # drag
            self.active_keyframe, self.active_timebead, self.keyframes_ori = drag(context, event, self.drag_mouse_ori, self.active_keyframe, self.active_handle, self.active_timebead, self.keyframes_ori, self.handles_ori)
        elif event.type == select + 'MOUSE' and event.value == 'CLICK':
            # select
            treshold = 10
            clicked = mathutils.Vector([event.mouse_region_x, event.mouse_region_y])
            self.active_keyframe = False
            self.active_handle = False
            self.active_timebead = False
            found = False
            for objectname, values in self.click.items():
                if found:
                    break
                for frame, type, coord in values:
                    if (coord - clicked).length <= treshold:
                        found = True
                        if type == "keyframe":
                            self.active_keyframe = [objectname, frame, frame]
                        elif type == "handle_left":
                            self.active_handle = [objectname, frame, "left"]
                        elif type == "handle_right":
                            self.active_handle = [objectname, frame, "right"]
                        elif type == "timebead":
                            self.active_timebead = [objectname, frame, frame]
                        break
        
        if context.area: # not available if other window-type is fullscreen
            context.area.tag_redraw()
        return {'PASS_THROUGH'}

    def invoke(self, context, event):
        if context.area.type == 'VIEW_3D':
            if context.window_manager.motion_trail.enabled == 0:
                # enable
                context.window_manager.motion_trail.enabled = 1
                self.active_keyframe = False
                self.active_handle = False
                self.active_timebead = False
                self.drag = False
                
                context.window_manager.modal_handler_add(self)
                self.handle1 = context.region.callback_add(calc_callback, (self, context), 'POST_VIEW')
                self.handle2 = context.region.callback_add(draw_callback, (self, context), 'POST_PIXEL')
            else:
                # disable
                context.window_manager.motion_trail.enabled = -1
            return {'RUNNING_MODAL'}
        
        else:
            self.report({'WARNING'}, "View3D not found, cannot run operator")
            return {'CANCELLED'}


class MotionTrailPanel(bpy.types.Panel):
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    bl_context = "objectmode"
    bl_label = "Motion Trail"

    def draw(self, context):
        col = self.layout.column()
        if context.window_manager.motion_trail.enabled != 1:
            col.operator("view3d.motion_trail", text="Enable motion trail")
        else:
            col.operator("view3d.motion_trail", text="Disable motion trail")
        
        box = self.layout.box()
        box.prop(context.window_manager.motion_trail, "mode")
        if context.window_manager.motion_trail.mode in ['timing']:
            box.prop(context.window_manager.motion_trail, "timebeads")
        
        box = self.layout.box()
        col = box.column()
        col.label("Path options")
        grouped = col.column(align=True)
        grouped.prop(context.window_manager.motion_trail, "path_width", text="Width")
        grouped.prop(context.window_manager.motion_trail, "path_transparency", text="Transparency")
        row = grouped.row(align=True)
        row.prop(context.window_manager.motion_trail, "path_before")
        row.prop(context.window_manager.motion_trail, "path_after")
        col.prop(context.window_manager.motion_trail, "keyframe_numbers")
        col.prop(context.window_manager.motion_trail, "path_style", text="Style")


class MotionTrailProps(bpy.types.PropertyGroup):
    enabled = bpy.props.IntProperty(default=0)
    keyframe_numbers = bpy.props.BoolProperty(name="Keyframe numbers", default=False, description="Display keyframe numbers")
    mode = bpy.props.EnumProperty(name="Mode",
        items=(("location", "Location", "Change path that is followed"),
            ("speed", "Speed", "Change speed between keyframes"),
            ("timing", "Timing", "Change position of keyframes on timeline")),
        description="Enable editing of certain properties in the 3d-view",
        default='location')
    path_after = bpy.props.IntProperty(name="After", min=0, default=0, description="Number of frames to show after the current frame, 0 = display all")
    path_before = bpy.props.IntProperty(name="Before", min=0, default=0, description="Number of frames to show before the current frame, 0 = display all")
    path_style = bpy.props.EnumProperty(name="Path style",
        items=(("acceleration", "Acceleration", "Gradient based on relative acceleration"),
            ("simple", "Simple", "Black line"),
            ("speed", "Speed", "Gradient based on relative speed")),
        description="Information conveyed by path colour",
        default='simple')
    path_transparency = bpy.props.IntProperty(name="Path transparency", min=0, max=100, default=0, subtype='PERCENTAGE', description="Determines visibility of path")
    path_width = bpy.props.IntProperty(name="Path width", min=1, soft_max=5, default=2, description="Width in pixels")
    timebeads = bpy.props.IntProperty(name="Time beads", min=1, soft_max = 10, default=5, description="Number of time beads to display per segment")


classes = [MotionTrailProps,
    MotionTrailOperator,
    MotionTrailPanel]


def register():
    for c in classes:
        bpy.utils.register_class(c)
    bpy.types.WindowManager.motion_trail = bpy.props.PointerProperty(type=MotionTrailProps)


def unregister():
    for c in classes:
        bpy.utils.unregister_class(c)
    del bpy.types.WindowManager.motion_trail


if __name__ == "__main__":
    register()