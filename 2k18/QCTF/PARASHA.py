file = open('text.txt', 'r')
X, Y = [], []
for i in range(22894):
    A = file.readline().split()[1:]
    if len(A) == 2:
        X.append(A[0][1:])
        Y.append(A[1][1:])


import matplotlib.pyplot as plt
graph = plt.figure() # Графический вывод
ax = graph.add_subplot(111)
ax.plot(X,Y,'bo', 'ro', ms=1)
plt.show()