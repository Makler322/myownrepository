n = int(input())
from functools import cmp_to_key
S = []
def gavno(a, b):
    c = a + b
    d = b + a
    if int(c) > int(d):
        return 1
    elif int(c) < int(d):
        return -1
    else:
        return 0
for i in range(n):
    S.append(input())
S.sort(key=cmp_to_key(gavno))
S.reverse()
print(''.join(map(str, S)))