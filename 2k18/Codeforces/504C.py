n, k = map(int, input().split())
A = input()
p = (n - k) // 2
m, o = 0, 0
for i in range(n):
    if A[i] == '(':
        if m < p:
            m += 1
        else:
            print('(', end='')
    else:
        if o < p:
            o += 1
        else:
            print(')', end='')
