n, q = map(int, raw_input().split())
K = n**2
L = n//2
P = (K + 1)//2
O = (K)//2
if n % 2 == 0:
    for i in range(q):
        a, b = map(int, raw_input().split())
        if (a + b) % 2 == 0:
            print (L) * (a - 1) + (b + 1)//2
        else:
            print O + (L) * (a - 1) + (b + 1)//2

else:
    for i in range(q):
        a, b = map(int, raw_input().split())
        if a % 2 == 1:
            if b % 2 == 1:
                print n * ((a - 1) // 2) + (b + 1)//2
            else:
                print P + n * ((a - 1) // 2) + (b + 1) // 2
        else:
            if b % 2 == 1:
                print P + n * ((a - 2) // 2) + (n - 1) // 2 + (b + 1) // 2
            else:
                print n * ((a - 2) // 2) + (n + 1) // 2 + (b + 1) // 2
