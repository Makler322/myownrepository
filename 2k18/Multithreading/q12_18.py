from threading import Thread
import queue



def inputer():  # Функция инпутера, котрая считывает данные и кладёт их очередь инпут,
                # если введено любое слово функция завершает свою работу, положив в очередь "Stop"
    while True:
        A = input().split()
        if len(A) < 3:
            for i in range(N):
                q_input.put([None, 'Stop', None])
            break
        else:
            a, b, c = A
            q_input.put([a, b, c])


def outputer(Stop_count, N):  # Функция оутпутера которая печатает то что лежит в очереди оутпут,
                 # но если встречаем "Stop" меняем счётчик на +1
    #global Stop_count, N
    while True:
        S = q_output.get()
        if S == 'Stop':
            Stop_count += 1
        else:
            print(S)
        if Stop_count == N:  # Если счетчик сравнялся с кол-во вореров - прекращаем работу программы
            break

def worker():  # Функция рабочего, которая достаёт данные из очереди инпут, инициализирует операцию,
               # выполняет её и кладёт результат в очередь оутпут
    while True:
        [a, b, c] = q_input.get()
        if b == '+':
            q_output.put(int(a) + int(c))
            #print('with +')
        elif b == '-':
            q_output.put(int(a) - int(c))
            #print('with -')
        elif b =='*':
            q_output.put(int(a) * int(c))
            # print('with *')
        elif b =='/':
            q_output.put(int(a) / int(c))
            # print('with /')
        elif b == 'Stop':  # Если операция подразумевает выход, то рабочий прекращает свою работу
            q_output.put(b)
            break
            
            
if __name__ == '__main__':
    N = 3
    Stop_count = 0
    q_output = queue.Queue()
    q_input = queue.Queue()

    T2 = [None]*N  # Создаем массив воркеров

    t1 = Thread(target=inputer) # Начинаем работу с потоками
    for i in range(N):
        T2[i] = Thread(target=worker)
    t3 = Thread(target=outputer,args = (Stop_count, N))

    t1.start()  # Запускаем все наши функции
    for i in range(N):
        T2[i].start()
    t3.start()


    t1.join() # Прекращяем работу
    for i in range(N):
        T2[i].join()
    t3.join()

