# from threading import Thread
import queue
import multiprocessing
import sys
import os


def inputer(q_input, fn, N):
    # Функция инпутера, котрая считывает данные и кладёт их очередь инпут,
    # если введено любое слово функция завершает свою работу, положив в очередь "Stop"
    sys.stdin = os.fdopen(fn)  # Чтение идёт через sys.stdin
    while True:
        A = input().split()
        if len(A) < 3:  # fixme подойди спроси на счет этой строчки
            for i in range(N):
                q_input.put([None, 'Stop', None])
            break
        else:
            a, b, c = A
            q_input.put([a, b, c])


def outputer(q_output, fn, N, Stop_count):
                # Функция оутпутера которая печатает то что лежит в очереди оутпут,
                 # но если встречаем "Stop" меняем счётчик на +1
    sys.stdin = os.fdopen(fn)
    #global Stop_count, N
    while True:
        S = q_output.get()
        if S == 'Stop':
            Stop_count += 1
        else:
            print(S)
        if Stop_count == N:
            break

def worker(q_input, q_output):
    # Функция рабочего, которая достаёт данные из очереди инпут, инициализирует операцию,
    # выполняет её и кладёт результат в очередь оутпут
    while True:
        [a, b, c] = q_input.get()
        if b == '+':
            q_output.put(int(a) + int(c))
            #print('with +')
        elif b == '-':
            q_output.put(int(a) - int(c))
            #print('with -')
        elif b =='*':
            q_output.put(int(a) * int(c))
            # print('with *')
        elif b =='/':
            q_output.put(int(a) / int(c))
            # print('with /')
        elif b == 'Stop':  # Если операция подразумевает выход, то рабочий прекращает свою работу
            q_output.put(b)
            break

#jjj
if __name__ == '__main__':
    N = 3
    Stop_count = 0
    # freeze_support()
    q_output = multiprocessing.Queue()
    q_input = multiprocessing.Queue()
    T2 = [None]*N # Создаем массив воркеров
    q = multiprocessing.Queue()
    fn = sys.stdin.fileno()  # get original file descriptor
    t1 = multiprocessing.Process(target=inputer, args=(q_input, fn, N)) # Начинаем работу с процессорами
    for i in range(N):
        T2[i] = multiprocessing.Process(target=worker, args=(q_input, q_output))
    t3 = multiprocessing.Process(target=outputer, args=(q_output, fn, N, Stop_count))

    t1.start() # Запускаем все наши функции
    for i in range(N):
        T2[i].start()
    t3.start()

    t1.join() # Прекращяем работу
    for i in range(N):
        T2[i].join()
    t3.join()
    

