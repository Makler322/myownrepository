def fa(a, l): #Функция бинарного поиска
    ai = 0
    bi = len(a) - 1
    while ai <= bi:
        k = (bi - ai + 1) // 2 + ai
        if a[k] > l:
            bi = k - 1
        elif a[k] == l:
            return l #Если мы нашли нужный нам элемент, то он выводится
        elif a[k] < l:
            ai = k + 1
    if a[k]!=l:
        if abs(a[k]-l) < abs(a[k-1]-l): #А если мы не нашли, то вывожу минимальное из разностей
            return a[k]
        else:
            return a[k-1] #Причём в случаи равенства беру минимальное значение k-1
c, d = map(int, input().split()) #По сути я не использую эти данные, ибо я знаю что такое len
a = list(map(int, input().split()))
b = list(map(int, input().split()))
for i in range(len(b)): #Обратим внимание, что если мы хотим оптимизировать программу по времени, то вместо len(b) можно написать d
   print(fa(a,b[i])) #Вся работа в 1 функции

