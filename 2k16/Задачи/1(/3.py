import PIL
import wave
import matplotlib

N,M,P=map(int,input().split())
s=[[]]
for i in range (N):
    a,b,c,d=map(int, input().split())
    for j in range(0,c):
        s[i].append(0)
    for l in range(c,d):
        s[i].append(1)
    for l in range(d, P):
        s[i].append(0)
print(s)