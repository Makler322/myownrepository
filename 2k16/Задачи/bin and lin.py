from time import time
from random import randint, choice
def fa(a,l): 
    ai=0
    bi=len(a)-1
    while ai<=bi:
        k=(bi-ai+1)//2+ai
        if a[k]>l:
            bi=k-1
        elif a[k]==l:
            break
        elif a[k]<l:
             ai=k+1
    if a[k]==l:
        return k
    else:
        return k+1
def linsearch(a,l):
    k=0
    while a[k]!=l:
        k+=1
    return k
for n in range(1000,10000,1000):
    a=[randint(-100000,100000) for i in range(n)]
    a.sort()
    vals=[choice(a) for j in range(1000)]
    t1=time()
    for val in vals:
        h=fa(a,val)
    t2=time()
    print('{n};{time:.8f}'.format(n=n, time=time()-t1),end=';')
    t1=time()
    for val in vals:
        e=linsearch(a,val)
    t2=time()
    print('{n};{time:.8f}'.format(n=n, time=time()-t1))