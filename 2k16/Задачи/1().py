def fa(a, l): #Функция бинарного поиска
    ai = 0
    bi = len(a) - 1
    while ai <= bi:
        k = (bi - ai + 1) // 2 + ai
        if a[k] > l:
            bi = k - 1
        elif a[k] == l:
            return k #Если мы нашли нужный нам элемент, то он выводится, а если его нет, то ничего не выводится :(
        elif a[k] < l:
            ai = k + 1
    return k
c, d = map(int, input().split()) #По сути я не использую эти данные, ибо я знаю что такое len
a = list(map(int, input().split()))
b = list(map(int, input().split()))
for i in range(len(b)): #Обратим внимание, что если мы хотим оптимизировать программу по времени, то вместо len(b) можно написать d
    if a[fa(a, b[i])]==b[i]:
        print(a[fa(a, b[i])])
    elif abs(a[fa(a, b[i])]-b[i])<abs(a[fa(a, b[i])-1]-b[i]):
        print(a[fa(a, b[i])])
    else:
        print(a[fa(a, b[i]) - 1])