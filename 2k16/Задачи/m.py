DEBUG = True
def vvod(n): #К сожалению эту функцию нельзя проверить, т.к. на вход подаётя кол-во строчек,
    s=[]     #а на выход список строк из чисел которые ввёл пользователь
    for i in range (0,n):
        s.append(list(map(int, input().split())))
    return s
def peremnozenie(A,B,n,m,k): #На вход  требуется аж 5 параметров, это 2 матрицы, которые мы хоти перемножить, длина и ширина первой и второй, прчём ширина первой и длина второй должны совпадать.
    """
    >>> peremnozenie([[1, 2], [3, 4]],[[1, 0], [0, 1]],2,2,2)
    [[1, 2], [3, 4]]
    >>> peremnozenie([[1, 2]],[[3], [4]],1,2,1)
    [[11]]
    """
    s=[[] for i in range(n)]
    c=0
    for i in range(n):
        for o in range(k):
            for u in range(m):
                c+=B[i][u]*A[u][o] #Мы складываем все эелементы m раз
            s[i].append(c)
            c=0 #Потом добавляем в новый список
    return s #На выходе получаем нужную матрицу
if __name__=='__main__': #Как-же без ДокТестов-то? ^_^
    if DEBUG:
        import doctest
        doctest.testmod()
    else:
        n,m=map(int,input().split())
        A=vvod(n)
        m,k=map(int,input().split())
        B=vvod(m)
        C=peremnozenie(A,B,n,m,k)
        for i in range(len(C)): #Из полученной матрицы выводим числа в том формате в котором нам нужно
            print(' '.join(map(str,C[i])))