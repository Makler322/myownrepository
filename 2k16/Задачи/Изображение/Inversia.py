from PIL import Image #Импортируем библиотеку PIL
img=Image.open ('5.jpg').convert('RGB') #Берём и открываем фото
h,l=img.size #Высота и длина
print(h,l)

for i in range(h): #Пробегаемся по высоте и длине
    for j in range(l):
        RGB=img.getpixel((i,j))
        r=RGB[0] #Узнаем параметры
        g=RGB[1]
        b=RGB[2]
        img.putpixel((i,j),(b,g,r)) #Для данного значения пикселя меняем параметры на противположные
img.save('New_Photo5.jpg') #Сохраняем
img.show() #Показываем
