DEBUG = True
def fa(l):
    """
    >>> fa(0)
    0
    >>> fa(170)
    6
    >>> fa(2)
    0
    >>> fa(160)
    5
    >>> fa(11)
    3
    >>> fa(23)
    4
    >>> fa(25)
    5
    >>> fa(3)
    1
    >>> fa(6)
    2
    """
    a=[2, 5, 11, 11, 23, 160]
    ai=0
    bi=len(a)-1
    while ai<=bi:
        k=(bi-ai+1)//2+ai
        if a[k]>l:
            bi=k-1
        elif a[k]==l:
            return k
        elif a[k]<l:
             ai=k+1
    if a[k]>l:
        return k
    else:
        return k+1
if __name__ == "__main__":
    if DEBUG:
        import doctest
        doctest.testmod()
    else:
        l=int(input())
        print(fa(l))