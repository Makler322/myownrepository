import matplotlib.pyplot as plt

def eiler(N0,k0,L,R): #Алгоритм Эйлера
    N1=N0
    N2=N0
    for i in range(1,11):
        N0=(1+(1-N0/L)*k0)*N0 #Алгоритм для каждой из формул
        N1=(1+(1-N1/L)*k0)*N1-R
        N2=(1+k0)*N2
        X.append(i)
        Y.append(round(N0))
        Y1.append(round(N1))
        Y2.append(round(N2))

N0=100
R=40
k0=0.5
L=1000

X,Y,Y1,Y2=[0],[N0],[N0],[N0]

eiler(N0,k0,L,R)


graph = plt.figure() # Графический вывод
ax = graph.add_subplot(111)
ax.plot(X,Y,'yellow')
ax.plot(X,Y1,'r')
ax.plot(X,Y2,'g')
print(X)
print(Y)
print(Y1)
print(Y2)
plt.show()
