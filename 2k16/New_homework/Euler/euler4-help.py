import matplotlib.pyplot as plt

def function(x): # Функция y(x), по х возвращает у
    return x**2

def pr(x): # Функция возращающая производную от х**2
    return 2*x

def eiler(x,y,k,step): #Алгоритм Эйлера
    okr=8 #Округление до okr знака
    X.append(k)
    while x<k:
        x+=step
        x=round(x,okr)
        y+=pr(x)*step
        y=round(y,okr)
    Y.append(round(round(abs(y-function(x)),okr)/function(x),okr))

X,Y=[],[]
x=1
y=1
k=11 # Конечный х
step=0.001
okr=8
for i in range(9):
    k-=1
    eiler(x,y,k,step)
graph = plt.figure() # Графический вывод
ax = graph.add_subplot(111)
ax.plot(X,Y)
print(X)
print(Y)
plt.show()