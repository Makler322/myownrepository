import math
a=float (input())
b=float (input())
c=float (input())
if a!=0:
    d=b**2-4*a*c
    if d>=0:
        n=math.sqrt(d)
        x=(-1*b-n)/(2*a)
        y=(-1*b+n)/(2*a)
        if (x*10)%10==0:
            x=int(x)
        if (y*10)%10==0:
            y=int(y)
        if x==y:
            print(1,x)
        else:
            print(2,min(x,y),max(x,y))
    else:
        print(0)
elif b!=0:
    x=(-c/b)
    if (x * 10) % 10 == 0:
        x = int(x)
    print(1, x)
elif c!=0:
    print(0)
else:
    print(3)